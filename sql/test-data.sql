# noinspection SqlWithoutWhereForFile
-- Use this file to add test data

use RetourSoiree;

\! echo 'Removing test data…';
delete
from CommentMention;
delete
from Comment;
delete
from Message;
delete
from Enrollment;
delete
from Student;
delete
from Travel;
delete
from Enrollment;
delete
from Event;
delete
from Place;
delete
from City;
delete
from Area;
delete
from City;

\! echo 'Filling Student…';
insert into Student(ID, first_name, last_name, email)
values (1, 'David', 'Gond', 'dgond@enseirb-matmeca.fr'),
       (2, 'Ivan', 'Canet', 'icanet@enseirb-matmeca.fr'),
       (3, 'Maëlle', 'Andricque', 'mandricque@enseirb-matmeca.fr'),
       (4, 'Mehdi', 'Ben Salah', 'mbensalah@enseirb-matmeca.fr');

\! echo 'Filling Area…';
insert into Area(ID, name)
values (1, 'Quinconces'),
       (2, 'Victoire'),
       (3, 'Saint-Genès');

\! echo 'Filling City…';
insert into City(ID, zip_code, name)
values (1, 33000, 'Bordeaux'),
       (2, 33400, 'Talence');

\! echo 'Filling Place…';
insert into Place(ID, address, area, city)
values (1, '66 avenue de Champs-Elysées', 1, 1),
       (2, '12 rue de la Paix', 2, 1),
       (3, '360 boulevard Jean Jaurès', 2, 2),
       (4, '221 chemin du théâtre', 3, 2),
       (5, '6 rue de la providence', 3, 1),
       (6, '15 voie des noyers', 3, 2);

\! echo 'Filling Event…';
insert into Event(ID, description, start_time, end_time, place)
values (1, 'qqchoseeirb', '2020-12-12 00:00:00', '2020-12-12 01:00:00', 1),
       (2, 'Ensqqchose', '2020-10-10 00:00:00', '2020-10-10 01:00:00', 2);

\! echo 'Filling Travel…';
insert into Travel(ID, start_time, dep_place, arr_place, event)
values (1, timestamp('2020-10-06', '9:30'), 1, 2, null),
       (2, timestamp('2020-06-10', '10:45'), 6, 3, null),
       (3, timestamp('2021-11-09', '20:22'), 2, 4, null),
       (4, timestamp('2019-01-25', '11:56'), 2, 3, 1),
       (5, timestamp('2019-10-25', '2:26'), 3, 1, 1),
       (6, timestamp('2021-08-12', '13:54'), 3, 4, 2),
       (7, timestamp('2020-12-23', '8:16'), 4, 5, 2),
       (8, timestamp('2020-09-30', '15:59'), 5, 3, null),
       (9, timestamp('2021-11-06', '16:23'), 5, 2, null);

\! echo 'Filling Enrollment…';
insert into Enrollment(ID, withdrawn, student, travel)
values (1, false, 1, 1),
       (2, false, 1, 3),
       (3, false, 1, 4),
       (4, true, 1, 2),
       (5, false, 2, 1),
       (6, false, 4, 1),
       (7, true, 4, 8),
       (8, false, 4, 9);

\! echo 'Filling Message…';
insert into Message(ID, enrollment, content, time)
values (1, 6, 'mehdi david', timestamp('2020-10-06', '10:00')),
       (2, 6, 'mehdi ivan', timestamp('2020-10-06', '10:00')),
       (3, 1, 'david ivan', timestamp('2020-10-06', '10:00')),
       (4, 1, 'david mehdi', timestamp('2020-10-06', '10:00')),
       (5, 5, 'ivan david', timestamp('2020-10-06', '10:00')),
       (6, 6, 'Bonjour', timestamp('2020-10-06', '8:00')),
       (7, 1, 'Salut', timestamp('2020-10-06', '8:10'));

\! echo 'Filling Comment…';
insert into Comment(ID, rating)
values (1, 5),
       (2, 4),
       (3, 5),
       (4, 4),
       (5, 5);

\! echo 'Filling CommentMention…';
insert into CommentMention(comment, student)
values (1, 1),
       (2, 2),
       (3, 2),
       (4, 4),
       (5, 1);

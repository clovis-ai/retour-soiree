-- Use this file to create the databases, triggers, etc

\! echo 'Resetting the schema…';

drop database if exists RetourSoiree;
create database RetourSoiree;
use RetourSoiree;

\! echo 'Creating Student…';
create table Student
(
	ID           int          not null auto_increment primary key,
	first_name   varchar(50)  not null,
	last_name    varchar(50)  not null,
	phone_number varchar(16),
	email        varchar(320) not null unique,

	constraint student_phone_number_null_or_plus_and_at_least_two_numbers check (phone_number is null or
	                                                                             (regexp_like(phone_number, '\\+[0-9]{2,}'))),
	constraint student_email_contains_at_sign check (email like '%@%')
);

\! echo 'Creating Area…';
create table Area
(
	ID   int         not null auto_increment primary key,
	name varchar(50) not null unique
);

\! echo 'Creating City…';
create table City
(
	ID       int         not null auto_increment primary key,
	zip_code int         not null,
	name     varchar(50) not null,

	constraint positive_city_zip_code check (zip_code > 0)
);

\! echo 'Creating Place…';
create table Place
(
	ID      int          not null auto_increment primary key,
	address varchar(200) not null,
	area    int          not null,
	city    int          not null,

	constraint fk_place_area foreign key (area) references Area (ID),
	constraint fk_place_city foreign key (city) references City (ID)
);

\! echo 'Creating Event…';
create table Event
(
	ID          int          not null auto_increment primary key,
	description varchar(127) not null,
	start_time  timestamp    not null,
	end_time    timestamp    not null,
	place       int,

	constraint fk_event_place foreign key (place) references Place (ID),
	constraint cmp_event_times check (start_time < end_time)
);

\! echo 'Creating Travel…';
create table Travel
(
	ID         int       not null auto_increment primary key,
	start_time timestamp not null,
	dep_place  int       not null,
	arr_place  int       not null,
	event      int,

	constraint fk_travel_from foreign key (dep_place) references Place (ID),
	constraint fk_travel_to foreign key (arr_place) references Place (ID),
	constraint fk_travel_event foreign key (event) references Event (ID)
);

\! echo 'Creating Enrollment…';
create table Enrollment
(
	ID        int     not null auto_increment primary key,
	withdrawn boolean not null default false,
	student   int     not null,
	travel    int     not null,

	constraint fk_enroll_student foreign key (student) references Student (ID),
	constraint fk_enroll_travel foreign key (travel) references Travel (ID)
);

\! echo 'Creating Message…';
create table Message
(
	ID         int          not null auto_increment primary key,
	enrollment int          not null,
	content    varchar(280) not null,
	time       timestamp    not null,

	constraint fk_message_enroll foreign key (enrollment) references Enrollment (ID)
);

delimiter $$
create trigger MessageTime
	before insert
	on Message
	for each row
begin
	declare enrollment_id integer;
	declare travel_start date;
	set enrollment_id = new.enrollment;
	set travel_start = (select start_time
	                    from Travel
	                    join Enrollment on Travel.ID = Enrollment.travel
	                    join Message on Enrollment.ID = Message.enrollment
	                    where Message.ID = enrollment_id);
	if (new.time >= date(travel_start) + interval 1 week)
	then
		set new = 'Error: Cannot add a message more than a week after the end of the event.';
	end if;
end;
$$
delimiter ;

\! echo 'Creating Comment…';
create table Comment
(
	ID     int not null primary key,
	rating int,

	constraint fk_comment_message foreign key (ID) references Message (ID),
	constraint cmp_comment_rating check (rating between 0 and 5)
);

delimiter $$
create trigger CommentTime
	before insert
	on Comment
	for each row
begin
	declare message_id int;
	declare travel_start date;
	declare message_time date;
	set message_id = new.ID;
	set travel_start = (select start_time
	                    from Travel
	                    join Enrollment on Travel.ID = Enrollment.travel
	                    join Message on Enrollment.ID = Message.enrollment
	                    where Message.ID = message_id);
	set message_time = (select time
	                    from Message
	                    where Message.ID = message_id);
	if (message_time < travel_start)
	then
		set new = 'Error: Cannot add a comment before the start of the corresponding travel.';
	end if;
end;
$$
delimiter ;

\! echo 'Creating CommentMention…';
create table CommentMention
(
	comment int not null,
	student int not null,

	primary key (comment, student),
	constraint fk_commentMention_comment foreign key (comment) references Comment (ID),
	constraint fk_commentMention_student foreign key (student) references Student (ID)
);

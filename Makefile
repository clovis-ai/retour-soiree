# ************************** Configuration *********************************** #
MYSQL_VERSION=mysql-8.0.22-linux-glibc2.12-x86_64
MYSQL_USER=root
MYSQL_PASSWORD=mysql
MYSQL_PORT=3306
GREEN=\033[0;32m
RED=\033[0;31m
RESET=\033[0m

# **************************** MySQL tasks *********************************** #
MYSQL_CACHE=.mysql
MYSQL_ARCHIVE=$(MYSQL_CACHE)/mysql.tar.xz
MYSQL_DIR=$(MYSQL_CACHE)/install
MYSQL_PATH_SERVER=$(MYSQL_DIR)/bin/mysqld --datadir=$(shell pwd)/$(MYSQL_DATA)
MYSQL_PATH=$(MYSQL_DIR)/bin/mysql
MYSQL_DATA=$(MYSQL_CACHE)/data
MYSQL_INIT_DB=$(MYSQL_CACHE)/init
MYSQL_TEST_DB=$(MYSQL_CACHE)/test

MYSQL_INIT_SCRIPTS=$(shell pwd)/sql

$(MYSQL_CACHE):
	@echo -e "${GREEN}Creating MySQL cache…${RESET}"
	mkdir -p $(MYSQL_CACHE)

$(MYSQL_ARCHIVE): $(MYSQL_CACHE)
ifneq ($(wildcard $(MYSQL_ARCHIVE)), $(MYSQL_ARCHIVE)) # Test if the file doesn't exist
	@echo -e "${GREEN}Downloading MySQL…${RESET}"
	wget --output-document=$@ https://dev.mysql.com/get/Downloads/MySQL-8.0/$(MYSQL_VERSION).tar.xz
endif

$(MYSQL_DIR): $(MYSQL_ARCHIVE)
	@echo -e "${GREEN}Extracting MySQL…${RESET}"
	tar xvf $(MYSQL_ARCHIVE)
	mv $(MYSQL_VERSION) $@

$(MYSQL_DATA): $(MYSQL_DIR)
	@echo -e "${GREEN}Initializing MySQL…${RESET}"
	rm -rf $(MYSQL_DATA)
	$(MYSQL_PATH_SERVER) --initialize --init-file=$(MYSQL_INIT_SCRIPTS)/startup.sql

.PHONY: mysql-start
mysql-start: $(MYSQL_DATA)
ifeq ($(shell lsof -i:$(MYSQL_PORT) | grep LISTEN),)
	@echo -e "${GREEN}Starting MySQL…${RESET}"
	$(MYSQL_PATH_SERVER) --user=${USER} &
	@sleep 20
else
	@echo -e "${GREEN}MySQL is already running:${RESET}"
	@lsof -i:$(MYSQL_PORT)
endif

$(MYSQL_INIT_DB): $(MYSQL_INIT_SCRIPTS)/init.sql | mysql-start
	@echo -e "${GREEN}Creating tables…${RESET}"
	<$(MYSQL_INIT_SCRIPTS)/init.sql $(MYSQL_PATH) --user=$(MYSQL_USER) --password=$(MYSQL_PASSWORD)
	touch $@

.PHONY:$(MYSQL_TEST_DB)
$(MYSQL_TEST_DB): $(MYSQL_INIT_DB) $(MYSQL_INIT_SCRIPTS)/test-data.sql | mysql-start
	@echo -e "${GREEN}Populating database…${RESET}"
	<$(MYSQL_INIT_SCRIPTS)/test-data.sql $(MYSQL_PATH) --user=$(MYSQL_USER) --password=$(MYSQL_PASSWORD)
	touch $@

# *************************** Gradle tasks *********************************** #
.PHONY: run test coverage javadoc gradle-clean

run: $(MYSQL_TEST_DB)
	@echo -e "${GREEN}Running with Gradle…${RESET}"
	./gradlew run

test: $(MYSQL_TEST_DB)
	@echo -e "${GREEN}Testing with Gradle…${RESET}"
	./gradlew test

coverage: $(MYSQL_TEST_DB)
	@echo -e "${GREEN}Generating test coverage report…${RESET}"
	./gradlew jacocoTestReport

javadoc:
	@echo -e "${GREEN}Compiling the documentation…${RESET}"
	./gradlew javadoc

gradle-clean:
	@echo -e "${GREEN}Cleaning with Gradle…${RESET}"
	./gradlew clean

# **************************** LaTeX tasks *********************************** #
LATEX_TEMPLATES_TOKEN=report/templates/README.md
LATEX_BUILD=report/templates/build

$(LATEX_TEMPLATES_TOKEN):
ifneq ($(wildcard $(LATEX_TEMPLATES_TOKEN)), $(LATEX_TEMPLATES_TOKEN)) # Test if the file doesn't exist
	@echo -e "${GREEN}Updating the templates…${RESET}"
	git submodule update --init
	ln -s templates/clovisai.sty report/clovisai.sty 2>/dev/null || true
	ln -s templates/clovisai-constructs.sty report/clovisai-constructs.sty 2>/dev/null || true
endif

report/report.pdf: $(LATEX_TEMPLATES_TOKEN) report/report.tex
	@echo -e "${GREEN}Compiling the report…${RESET}"
	cd report && ./templates/build PDF report.tex

report.pdf: report/report.pdf
	cp report/report.pdf report.pdf

# *************************** Deposit tasks ********************************** #
DEPOSIT=/tmp/rendu

$(DEPOSIT).zip: report/report.pdf
	@echo -e "${GREEN}Generating the archive…${RESET}"
	rm -rf $(DEPOSIT).zip $(DEPOSIT)
	mkdir -p $(DEPOSIT)
	cp README.md $(DEPOSIT)/README.md
	cp report/report.pdf $(DEPOSIT)/rapport.pdf

	mkdir -p $(DEPOSIT)/sql
	cp sql/* $(DEPOSIT)/sql/
	for f in src/main/resources/sql/*; do echo -e "\n-- $$f" >>$(DEPOSIT)/sql/requests.sql; cat "$$f" >>$(DEPOSIT)/sql/requests.sql; done

	r=$(shell pwd) && cd $(DEPOSIT) && git clone $$r code
	rm -rf $(DEPOSIT)/code/.git $(DEPOSIT)/code/.idea

	@echo -e "${GREEN}Compressing…${RESET}"
	zip -r $(DEPOSIT) $(DEPOSIT)

# ************************* Cleaning up tasks ******************************** #
.PHONY: clean uninstall

clean: gradle-clean

uninstall: clean
	rm -rf $(MYSQL_CACHE)
	rm -rf .gradle

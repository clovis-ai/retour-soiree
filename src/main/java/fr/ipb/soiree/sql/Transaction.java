package fr.ipb.soiree.sql;

import com.jcabi.log.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;

/**
 * A SQL transaction.
 *
 * <p>A transaction should be closed, either by:
 * <ul>
 *     <li>Validating it with {@link #commit()}</li>
 *     <li>Cancelling it with {@link #abort()}</li>
 * </ul>
 * <p>
 * You could, for example, write:
 * <pre>
 *     Transaction t = …;
 *     try {
 *         t.select(…);
 *         t.update(…);
 *         t.commit();
 *     } catch (SQLException e) {
 *         t.abort();
 *     }
 * </pre>
 * Or, as the {@link #close()} method is an alias for {@link #abort()},
 * using Java's <code>try-with-resources</code> statement:
 * <pre>
 *     Transaction t = …;
 *     try (t) {
 *         t.select(…);
 *         t.update(…);
 *         t.commit();
 *     } catch (SQLException e) {}
 * </pre>
 */
public class Transaction implements AutoCloseable {

	private final Connection connection;
	private final Map<Request, PreparedStatement> prepared;

	Transaction(Connection connection, Map<Request, PreparedStatement> prepared) {
		this.connection = connection;
		this.prepared = prepared;
	}

	/**
	 * Executes a <code>SELECT</code> request.
	 *
	 * @param request    the request
	 * @param parameters a lambda that applies the parameters you want
	 * @return the results of the <code>SELECT</code> request (see {@link
	 * PreparedStatement#executeQuery()}).
	 * @throws SQLException If anything goes wrong during the request
	 * @see #select(Request) SELECT without parameters
	 * @see #update(Request, SetParameters) Other SQL requests
	 */
	public ResultSet select(Request request, SetParameters parameters) throws SQLException {
		Logger.debug(this, "SELECT " + request);
		return setupStatement(request, parameters).executeQuery();
	}

	/**
	 * Executes a <code>SELECT</code> request without parameters.
	 *
	 * @param request the request
	 * @return the results of the <code>SELECT</code> request (see {@link
	 * PreparedStatement#executeQuery()}).
	 * @throws SQLException If anything goes wrong during the request
	 * @see #select(Request, SetParameters) SELECT with parameters
	 * @see #update(Request, SetParameters) Other SQL requests
	 */
	public ResultSet select(Request request) throws SQLException {
		return select(request, null);
	}

	/**
	 * Executes a <code>UPDATE</code>, <code>INSERT</code>, <code>DELETE</code>, or any other
	 * request that modifies data.
	 *
	 * @param request    the request
	 * @param parameters a lambda that applies the parameters you want
	 * @return the number of rows that were matched by the request (see {@link
	 * PreparedStatement#executeUpdate()})
	 * @throws SQLException if anything goes wrong during the request
	 * @see #select(Request, SetParameters) SELECT SQL requests
	 */
	public int update(Request request, SetParameters parameters) throws SQLException {
		Logger.debug(this, "UPDATE " + request);
		return setupStatement(request, parameters).executeUpdate();
	}

	private PreparedStatement setupStatement(Request request, SetParameters parameters) throws SQLException {
		PreparedStatement statement = prepared.get(request);
		Objects.requireNonNull(statement,
		                       "This statement hasn't been prepared, it's not possible to execute it now: " + request);

		statement.clearParameters();
		if (parameters != null)
			parameters.setParameters(statement);
		return statement;
	}

	/**
	 * Confirm the transaction, and applies it to the database.
	 *
	 * @throws SQLException If the database refuses.
	 * @see #abort()
	 */
	public void commit() throws SQLException {
		Logger.debug(this, "Committing");
		connection.commit();
	}

	/**
	 * Closes and backtracks the transaction, so it never happened.
	 *
	 * @throws SQLException If the database refuses.
	 * @see #commit()
	 */
	public void abort() throws SQLException {
		Logger.debug(this, "Aborting");
		connection.rollback();
	}

	/**
	 * Closes and fails the transaction.
	 *
	 * @see #abort()
	 */
	@Override
	public void close() throws SQLException {
		abort();
	}

	/**
	 * Returns the last inserted id. Has limitations when used in concurrency or insert multiple
	 * rows at once.
	 *
	 * @return The last inserted id.
	 * @throws SQLException If an error occurred
	 * @see <a href="https://dev.mysql.com/doc/refman/8.0/en/information-functions.html#function_last-insert-id">Link
	 * to MySQL doc page</a>
	 */
	public long lastId() throws SQLException {
		ResultSet result = select(Request.GENERAL_LAST_INSERT_ID);

		if (result.next())
			return result.getLong(1);
		else
			throw new IllegalStateException("Could not retrieve the last inserted id.");
	}

	/**
	 * A small SAM interface used to inject parameters into a prepared request.
	 */
	@FunctionalInterface
	public interface SetParameters {

		void setParameters(PreparedStatement statement) throws SQLException;
	}
}

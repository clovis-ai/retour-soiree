package fr.ipb.soiree.sql;

import com.jcabi.log.Logger;
import fr.ipb.soiree.sql.Transaction.SetParameters;

import java.sql.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class that encapsulates database access.
 *
 * <p>Do not forget to call {@link #close()} when you're done with the database.</p>
 *
 * <p>There are two ways to instantiate this object:
 * <ul>
 *     <li>By giving all information, using {@link #Database(String, int, String, String)}</li>
 *     <li>Automatically, using {@link #connectAuto()}</li>
 * </ul>
 * <p>
 * Use {@link #start()} to create a {@link Transaction}.
 */
public class Database implements AutoCloseable {

	private final String url;
	private final Connection connection;
	private final Map<Request, PreparedStatement> prepared;

	/**
	 * Connects to the database.
	 *
	 * @param host     on which machine the database is running
	 * @param port     on which port of that machine the database is running
	 * @param user     the SQL user that is used to access the database
	 * @param password the password of that SQL user
	 * @see #connectAuto() Connect automatically
	 */
	Database(String host, int port, String user, String password) {
		url = "jdbc:mysql://" + host + ":" + port
				+ "/RetourSoiree?user=" + user + "&password=" + password;

		loadDriver();
		connection = connect(url);
		disableAutoCommit();
		prepared = prepareRequests();
	}

	/**
	 * Use reflection to get an instance of the JDBC driver.
	 *
	 * @see <a href="https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-usagenotes-connect-drivermanager.html">MySQL
	 * documentation</a>
	 */
	private void loadDriver() {
		final String driverName = "com.mysql.cj.jdbc.Driver";
		Logger.trace(this, "Loading the driver %s", driverName);
		try {
			Class.forName(driverName)
					.getDeclaredConstructor()
					.newInstance();
		} catch (ReflectiveOperationException e) {
			throw new RuntimeException("Could not instantiate the MySQL driver.", e);
		}
	}

	/**
	 * Connect to the database.
	 *
	 * @param jdbcUrl The JDBC URL that should be used to connect, including any necessary
	 *                password.
	 * @return A connection
	 */
	private Connection connect(String jdbcUrl) {
		Logger.trace(this, "Connecting to the database %s", jdbcUrl);
		try {
			return DriverManager.getConnection(jdbcUrl);
		} catch (SQLException e) {
			throw new RuntimeException("Could not connect to the database '" + jdbcUrl + "'", e);
		}
	}

	private void disableAutoCommit() {
		Logger.trace(this, "Disabling auto-commit");
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			throw new RuntimeException("Could not disable auto-commit.", e);
		}
	}

	private Map<Request, PreparedStatement> prepareRequests() {
		Stream<Request> requests = Arrays.stream(Request.values());

		Function<Request, PreparedStatement> getStatement = request -> {
			Logger.trace(this, "Preparing request %s", request);
			try {
				return connection.prepareStatement(request.text);
			} catch (SQLException e) {
				throw new RuntimeException("Could not prepare request " + request, e);
			}
		};

		return requests.collect(Collectors.toMap(Function.identity(), getStatement));
	}

	public Transaction start() {
		return new Transaction(connection, prepared);
	}

	/**
	 * Automatically connect to the database.
	 *
	 * @return A working connection
	 */
	public static Database connectAuto() {
		return new Database("localhost", 3306, "root", "mysql");
	}

	/**
	 * Disconnect from the database.
	 */
	@Override
	public void close() {
		Logger.trace(this, "Disconnecting from the database %s", url);
		try {
			connection.close();
		} catch (SQLException e) {
			throw new RuntimeException("Could not close the connection.", e);
		}
	}

	/**
	 * Creates a list of objects from a SQL request.
	 *
	 * @param request    The request (must be a <code>SELECT</code>)
	 * @param parameters The parameters, if needed
	 * @param mapper     A function that associates a row to an object
	 * @param <T>        The type of objects we want to list
	 * @return A list of objects
	 * @throws SQLException If something bad happens
	 * @see #selectAll(Request, SqlMapper) Without parameters
	 */
	public <T> List<T> selectAll(Request request, SetParameters parameters, SqlMapper<T> mapper) throws SQLException {
		var ret = new ArrayList<T>();

		try (var transaction = start()) {
			try (var results = transaction.select(request, parameters)) {
				while (results.next())
					ret.add(mapper.map(results));
			}
		}

		return ret;
	}

	/**
	 * Creates a list of objects from a SQL request.
	 *
	 * @param request The request (must be a <code>SELECT</code>)
	 * @param mapper  A function that associates a row to an object
	 * @param <T>     The type of objects we want to list
	 * @return A list of objects
	 * @throws SQLException If something bad happens
	 * @see #selectAll(Request, SetParameters, SqlMapper) With parameters
	 */
	public <T> List<T> selectAll(Request request, SqlMapper<T> mapper) throws SQLException {
		return selectAll(request, null, mapper);
	}

	/**
	 * Creates an object from a SQL request.
	 *
	 * @param request    The request (must be a <code>SELECT</code>)
	 * @param parameters The parameters of the request
	 * @param mapper     A function that associates a row to
	 * @param <T>        The type of the object we want returned
	 * @return Either an optional of the value we want, or an empty optional if none was found.
	 * @throws SQLException          If something went wrong with the SQL request.
	 * @throws IllegalStateException If more than one result was found.
	 */
	public <T> Optional<T> selectOne(Request request, SetParameters parameters, SqlMapper<T> mapper) throws SQLException {
		Optional<T> result;

		try (var transaction = start()) {
			try (var results = transaction.select(request, parameters)) {
				if (results.next()) {
					result = Optional.of(mapper.map(results));

					if (results.next())
						throw new IllegalStateException(
								"Found a second result for request " + request + ", but zero or one result were expected: " + results);

				} else {
					result = Optional.empty();
				}
			}
		}

		return result;
	}

	/**
	 * Transform a row of data into an object.
	 *
	 * @param <T> The object that should be created.
	 */
	@FunctionalInterface
	public interface SqlMapper<T> {

		T map(ResultSet results) throws SQLException;
	}
}

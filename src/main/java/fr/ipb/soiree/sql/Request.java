package fr.ipb.soiree.sql;

import com.jcabi.log.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * All the SQL requests used by this project.
 */
public enum Request {
	GENERAL_LAST_INSERT_ID("sql/general-last-insert-id.sql"),
	CITY_ID("sql/city-id.sql"),
	CITY_ALL("sql/city-all.sql"),
	CITY_INSERT("sql/city-insert.sql"),
	STUDENTS_ALL("sql/students-all.sql"),
	STUDENTS_ID("sql/students-with-id.sql"),
	STUDENTS_FIRST_NAME("sql/students-with-first-name.sql"),
	STUDENTS_LAST_NAME("sql/students-with-last-name.sql"),
	STUDENTS_FULL_NAME("sql/students-with-full-name.sql"),
	STUDENTS_EMAIL("sql/students-with-email.sql"),
	STUDENTS_INSERT("sql/students-insert.sql"),
	STUDENTS_TRAVELLED_TOGETHER("sql/students-travelled-together.sql"),
	EVENT_ALL("sql/event-all.sql"),
	EVENT_INSERT("sql/event-insert.sql"),
	ALL_AREAS_WITH_PLACES("sql/all-areas-with-places.sql"),
	AREAS_ID("sql/areas-id.sql"),
	AREAS_ALL("sql/areas-all.sql"),
	AREAS_INSERT("sql/areas-insert.sql"),
	MOST_POPULAR_PLACES("sql/most-popular-places.sql"),
	PLACE_WITH_ID("sql/place-with-id.sql"),
	PLACE_ALL("sql/place-all.sql"),
	PLACE_INSERT("sql/place-insert.sql"),
	EVENT_WITH_ID("sql/event-with-id.sql"),
	TRAVELS_WITH_AREAS_AND_TIME("sql/travels-with-areas-and-time.sql"),
	TRAVELS_WITH_ID("sql/travels-with-id.sql"),
	TRAVELS_WITHOUT_EVENT("sql/travels-without-event.sql"),
	STUDENT_TRAVELS_NOT_WITHDRAWN("sql/student-travels-not-withdrawn.sql"),
	TRAVEL_ALL("sql/travels-all.sql"),
	TRAVELS_WITH_DEP_AREA("sql/travels-with-departure-area.sql"),
	TRAVELS_WITH_ARR_AREA("sql/travels-with-arrival-area.sql"),
	TRAVELS_WITH_DATE("sql/travels-with-date.sql"),
	TRAVELS_WITH_EVENT("sql/travels-with-event.sql"),
	STUDENTS_BY_TRAVEL("sql/students-by-travel.sql"),
	ALL_STUDENTS_WITH_TRAVELS_AND_GRADE("sql/all-students-with-travels-and-grade.sql"),
	ENROLLMENT_WITH_ID("sql/enrollment-with-id.sql"),
	MESSAGES_WITH_TRAVEL("sql/messages-with-travel.sql"),
	MESSAGE_INSERT("sql/message-insert.sql"),
	ENROLLMENT_UNWITHDRAW("sql/enrollment-unwithdraw.sql"),
	ENROLLMENT_WITHDRAW("sql/enrollment-withdraw.sql"),
	ENROLLMENT_INSERT("sql/enrollment-insert.sql"),
	ENROLLMENT_WITH_STUDENT_AND_TRAVEL("sql/enrollment-with-student-and-travel.sql"),
	COMMENTS_INSERT("sql/comments-insert.sql"),
	COMMENTS_WITH_TRAVEL("sql/comments-with-travel.sql"),
	COMMENTMENTIONS_INSERT("sql/comment-mentions-insert.sql"),
	TRAVELS_INSERT("sql/travels-insert.sql"),
	ALL_COMMENTS_ABOUT_ME("sql/all-comments-about-me.sql");

	/**
	 * The file this request is stored in.
	 */
	public final String file;

	/**
	 * The textual representation of the request. Use this to pass the request to JDBC.
	 */
	public final String text;

	Request(String filename) {
		file = filename;
		text = read();
	}

	private String read() {
		Logger.trace(this, "Reading script '%s'", file);

		InputStream resource = getClass().getClassLoader().getResourceAsStream(file);
		Objects.requireNonNull(resource, "Could not find the resource " + file);

		BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
		Stream<String> lines = reader.lines();

		return lines.collect(Collectors.joining("\n"));
	}

	@Override
	public String toString() {
		return name() + '(' + file + ')';
	}
}

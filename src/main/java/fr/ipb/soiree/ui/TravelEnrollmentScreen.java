package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.*;
import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.utils.JLabelUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public class TravelEnrollmentScreen extends JDialog {

	private final Database db;

	private JLabel title;
	private final Student user;
	private final Supplier<List<Travel>> getTravels;
	private FormattedTable table;

	public TravelEnrollmentScreen(Database db, Student user, Supplier<List<Travel>> getTravels) {
		this.db = Objects.requireNonNull(db);
		this.user = Objects.requireNonNull(user);
		this.getTravels = Objects.requireNonNull(getTravels);

		Logger.trace(this, "Initializing the travel Enrollment screen");

		setLayout(new BorderLayout());
		setTitle("Liste de trajets");
		draw();
	}

	private void draw() {
		var quit = new JButton("Quitter");
		quit.setPreferredSize(new Dimension(200, 40));
		quit.addActionListener(this::onQuit);

		var travels = getTravels.get();
		if (travels.isEmpty()) {
			title = new JLabel("Aucun trajet ne correspond aux critères");
			JLabelUtilities.resizeLabel(title, 20);
			title.setHorizontalAlignment(JLabel.CENTER);
			add(title, BorderLayout.CENTER);
			add(quit, BorderLayout.SOUTH);
		} else {
			var northPane = new JPanel(new BorderLayout());
			title = new JLabel("Choisir un trajet parmi " + travels.size() + " possible·s :");
			JLabelUtilities.resizeLabel(title, 20);
			title.setHorizontalAlignment(JLabel.CENTER);
			northPane.add(title, BorderLayout.NORTH);

			var model = new SoireeTableModel.TravelTableModel(travels);
			table = new FormattedTable(model);

			var southPane = new JPanel(new GridLayout(1, 5));
			southPane.add(quit);

			var widthdraw = new JButton("Se désinscrire");
			widthdraw.setPreferredSize(new Dimension(200, 40));
			widthdraw.addActionListener(this::onWithdraw);
			southPane.add(widthdraw);

			var enroll = new JButton("S'inscrire");
			enroll.setPreferredSize(new Dimension(200, 40));
			enroll.addActionListener(this::onEnrollment);
			southPane.add(enroll);

			var message = new JButton("Messages");
			message.setPreferredSize(new Dimension(200, 40));
			message.addActionListener(this::onMessage);
			southPane.add(message);

			var commentaire = new JButton("Commentaires");
			commentaire.setPreferredSize(new Dimension(200, 40));
			commentaire.addActionListener(this::onComment);
			southPane.add(commentaire);

			add(northPane, BorderLayout.NORTH);
			add(table, BorderLayout.CENTER);
			add(southPane, BorderLayout.SOUTH);
		}
		setSize(1000, 600);
		setVisible(true);
	}

	private void resetFrame() {
		getContentPane().removeAll();
		getContentPane().validate();
		getContentPane().repaint();
		draw();
	}

	private Optional<Travel> getSelectedTravel() throws SQLException {
		var tableValues = table.getTable();
		var row = tableValues.getSelectedRow();
		var id = (long) tableValues.getValueAt(row, 0);
		return Travel.withID(db, id);
	}

	private void withdraw(Database db, Student user, Travel travel) throws SQLException {
		var enrollment = Enrollment.withdraw(db, user, travel);
		String text;
		if (enrollment.isPresent())
			text = "Désinscription au trajet " + enrollment.get().getTravel().getId() +
					" réussie !";
		else
			text = "La désinscription au trajet " + travel.getId() + " n'a pas pu se faire.";
		JOptionPane.showMessageDialog(this, text);
	}

	private void enroll(Database db, Student user, Travel travel) throws SQLException {
		Enrollment.enroll(db, user, travel);
		JOptionPane.showMessageDialog(this,
		                              "Inscription au trajet " + travel.getId() + " réussie !");
	}

	private void onEnrollment(ActionEvent e) {
		Logger.trace(this, "The user wants to enroll to a travel.");

		try {
			var travelDB = getSelectedTravel();
			if (travelDB.isPresent()) {
				var travel = travelDB.get();
				var enrollment = Enrollment.withStudentAndTravel(db, user, travel);
				// Enroll only if no previous enrollment or is withdrawn
				if (enrollment.isEmpty() || enrollment.get().isWithdrawn()) {
					enroll(db, user, travel);
					resetFrame();
				}
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText("Impossible de vérifier les trajets.");
		}
	}

	private void onWithdraw(ActionEvent e) {
		Logger.trace(this, "The user wants to withdraw to a travel.");

		try {
			var travelDB = getSelectedTravel();
			if (travelDB.isPresent()) {
				var travel = travelDB.get();
				var enrollment = Enrollment.withStudentAndTravel(db, user, travel);
				// Withdraw only if a previous enrollment exists
				if (enrollment.isPresent()) {
					withdraw(db, user, travel);
					resetFrame();
				}
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText("Impossible de vérifier les trajets.");
		}
	}

	private void onMessage(ActionEvent e) {
		Logger.trace(this, "The user wants to go to the Message screen of a travel.");

		try {
			var travelDB = getSelectedTravel();
			if (travelDB.isPresent()) {
				var travel = travelDB.get();
				var enrollment = Enrollment.withStudentAndTravel(db, user, travel);
				if (enrollment.isPresent() && !enrollment.get().isWithdrawn()) {
					var messages = Message.withTravel(db, travel);
					new MessagesOfTravel(db, travel, user, messages);
				} else {
					title.setText("Impossible d'afficher le message : étudiant non inscrit");
				}
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText("Impossible d'afficher les messages.");
		}
	}

	private void onComment(ActionEvent e) {
		Logger.trace(this, "The user wants to go to the Comments screen of a travel.");

		try {
			var travelDB = getSelectedTravel();
			if (travelDB.isPresent()) {
				final var enrollment = Enrollment.withStudentAndTravel(db, user, travelDB.get());
				var travel = travelDB.get();
				var comments = Comment.withTravelComments(db, travel);

				enrollment.map(value -> new CommentsOfTravel(db, value, comments))
						.orElseThrow(() -> new SQLException("Found no matching enrollment"));
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText("Impossible d'afficher les messages.");
		}
	}

	private void onQuit(ActionEvent e) {
		Logger.debug(this, "Closing the enrollment page.");
		dispose();
	}
}

package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Event;
import fr.ipb.soiree.data.*;
import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.utils.JLabelUtilities;
import fr.ipb.soiree.utils.Pair;
import fr.ipb.soiree.utils.SpringUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * A user's main page, from where it can start all actions.
 */
public class AdminDashboard extends JFrame {

	private final Database db;

	public AdminDashboard(Database db) {
		Objects.requireNonNull(db);
		this.db = db;
		Logger.trace(this, "Initializing the admin page");

		var globalPane = new JPanel(new BorderLayout());
		var globalCenterPane = new JPanel(new GridBagLayout());

		var centerPane = new JPanel(new SpringLayout());

		var title = new JLabel("Page d'administration");
		JLabelUtilities.resizeLabel(title, 24);
		title.setHorizontalAlignment(JLabel.CENTER);
		globalPane.add(title, BorderLayout.NORTH);

		var gap = new JButton("1");
		gap.setVisible(false);

		var rows = 0;
		var allPlaces = new JButton("Tous les lieux");
		allPlaces.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		allPlaces.addActionListener(this::onAllPlaces);
		centerPane.add(allPlaces);
		rows++;

		var allAreas = new JButton("Toutes les zones géographiques");
		allAreas.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		allAreas.addActionListener(this::onAllAreas);
		centerPane.add(allAreas);
		rows++;

		var allTravels = new JButton("Tous les trajets");
		allTravels.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		allTravels.addActionListener(this::onAllTravels);
		centerPane.add(allTravels);
		rows++;

		var allCities = new JButton("Toutes les villes");
		allCities.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		allCities.addActionListener(this::onAllCities);
		centerPane.add(allCities);
		rows++;

		var allEvents = new JButton("Tous les événements");
		allEvents.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		allEvents.addActionListener(this::onAllEvents);
		centerPane.add(allEvents);
		rows++;

		centerPane.add(gap);
		rows++;

		var nbStudentsPerTravel = new JButton("Nombre d'étudiants inscrits par trajet");
		nbStudentsPerTravel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		nbStudentsPerTravel.addActionListener(this::onNbStudentsPerTravel);
		centerPane.add(nbStudentsPerTravel);
		rows++;

		var nbPlacesPerArea = new JButton("Nombre de lieux par zone");
		nbPlacesPerArea.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		nbPlacesPerArea.addActionListener(this::onNbPlacesPerArea);
		centerPane.add(nbPlacesPerArea);
		rows++;

		var nbTravelsAndAvgRatingPerStudent = new JButton("Résumé des trajets de chaque étudiant");
		nbTravelsAndAvgRatingPerStudent.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		nbTravelsAndAvgRatingPerStudent.addActionListener(this::onNbTravelsAndAvgRatingPerStudent);
		centerPane.add(nbTravelsAndAvgRatingPerStudent);
		rows++;

		var mostPopularPlacesPerArea = new JButton("Liste des lieux les plus populaires par zone");
		mostPopularPlacesPerArea.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		mostPopularPlacesPerArea.addActionListener(this::onMostPopularPlacesPerArea);
		centerPane.add(mostPopularPlacesPerArea);
		rows++;

		SpringUtilities.makeCompactGrid(centerPane, rows, 1, 5, 5, 5, 5);

		var quit = new JButton("Déconnexion");
		quit.addActionListener(this::onQuit);
		globalPane.add(quit, BorderLayout.SOUTH);

		globalCenterPane.add(centerPane, new GridBagConstraints());
		globalPane.add(globalCenterPane, BorderLayout.CENTER);
		add(globalPane, BorderLayout.CENTER);
		setSize(600, 600);

		setTitle("Administration");
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	private void onQuit(ActionEvent e) {
		Logger.debug(this, "Closing the admin page.");
		dispose();
	}

	private void onAllPlaces(ActionEvent evt) {
		try {
			var places = Place.all(db);
			new ListDisplay("Tous les lieux", new SoireeTableModel.PlaceTableModel(places));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onAllAreas(ActionEvent evt) {
		try {
			var areas = Area.all(db);
			new ListDisplay("Toutes les zones géographiques",
			                new SoireeTableModel.AreaTableModel(areas));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onAllTravels(ActionEvent evt) {
		try {
			var travels = Travel.all(db);
			new ListDisplay("Tous les trajets",
			                new SoireeTableModel.TravelTableModel(new ArrayList<>(travels)));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onAllCities(ActionEvent evt) {
		try {
			var cities = City.all(db);
			new ListDisplay("Toutes les villes",
			                new SoireeTableModel.CityTableModel(new ArrayList<>(cities)));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onAllEvents(ActionEvent evt) {
		try {
			var events = Event.all(db);
			new ListDisplay("Tous les événements",
			                new SoireeTableModel.EventTableModel(new ArrayList<>(events)));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onNbStudentsPerTravel(ActionEvent evt) {
		try {
			var travelsDB = Travel.studentsByTravel(db);
			var travels = new ArrayList<Pair<Travel, Integer>>();
			travelsDB.forEach((travel, number) -> {
				travels.add(new Pair<>(travel, number));
			});
			new ListDisplay("Nombre d'étudiants inscrits par trajet",
			                new SoireeTableModel.TravelCountingStudentTableModel(travels));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onNbPlacesPerArea(ActionEvent evt) {
		try {
			var areasDB = Area.allAreasWithPLaces(db);
			var areas = new ArrayList<Pair<Area, Integer>>();
			areasDB.forEach((area, number) -> {
				areas.add(new Pair<>(area, number));
			});
			new ListDisplay("Nombre de lieux par zone",
			                new SoireeTableModel.AreaCountingPlaceTableModel(areas));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onNbTravelsAndAvgRatingPerStudent(ActionEvent evt) {
		try {
			var studentsDB = Student.allStudentsWithTravelsAndGrade(db);
			var students = new ArrayList<Pair<Student, Pair<Integer, Float>>>();
			studentsDB.forEach((student, pair) -> {
				students.add(new Pair<>(student, pair));
			});
			new ListDisplay("Résumé des trajets de chaque étudiant",
			                new SoireeTableModel.StudentCountingTravelsAndAvgRatingTableModel(
					                students));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

	private void onMostPopularPlacesPerArea(ActionEvent evt) {
		try {
			var areasDB = Area.mostPopularPlaces(db);
			var areas = new ArrayList<Pair<Area, Set<Place>>>();
			areasDB.forEach((area, set) -> {
				areas.add(new Pair<>(area, set));
			});
			new ListDisplay("Lieu le plus populaire par zone géographique",
			                new SoireeTableModel.AreaMostPopularPlaceTableModel(areas));
		} catch (SQLException e) {
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
	}

}

package fr.ipb.soiree.ui;

import com.github.lgooddatepicker.components.DateTimePicker;
import com.jcabi.log.Logger;
import fr.ipb.soiree.data.FakeObjects;
import fr.ipb.soiree.data.Place;
import fr.ipb.soiree.data.Travel;
import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.utils.JLabelUtilities;
import fr.ipb.soiree.utils.SpringUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CreateTravelScreen extends JDialog {

	private final Database db;
	private final JLabel titleLabel;
	private final DateTimePicker start;
	private final JComboBox<Place> departurePlace;
	private final JComboBox<Place> arrivalPlace;
	private final JComboBox<fr.ipb.soiree.data.Event> relatedEvent;

	public CreateTravelScreen(Database db) {
		Objects.requireNonNull(db);
		this.db = db;

		Logger.trace(this, "Initializing the event creation screen.");

		var borderPane = new JPanel(new BorderLayout());
		var rows = 0;
		var cols = 2;

		/* Title */
		var title = "Nouveau trajet";
		titleLabel = new JLabel(title);
		titleLabel.setHorizontalAlignment(JLabel.CENTER);
		JLabelUtilities.resizeLabel(titleLabel, 24);
		borderPane.add(titleLabel, BorderLayout.NORTH);

		/* Form */
		var formLayout = new SpringLayout();
		var formPanel = new JPanel(formLayout);

		/* Start time */
		var startText = "Date de départ";
		var startLabel = new JLabel(startText + " : ");
		start = new DateTimePicker();
		start.datePicker.getSettings().setAllowEmptyDates(false);
		start.timePicker.getSettings().setAllowEmptyTimes(false);
		start.setToolTipText(startText);
		formPanel.add(startLabel);
		formPanel.add(start);
		rows++;

		var areas = loadPlaces();

		/* Departure Place */
		var departureText = "Lieu de départ";
		var departureLabel = new JLabel(departureText + " : ");
		departurePlace = new JComboBox<>(areas.toArray(new Place[0]));
		departurePlace.setMinimumSize(new Dimension(200, 20));
		departurePlace.setToolTipText(departureText);
		departurePlace.setRenderer(new ComboBoxRenderer.PlaceRenderer());
		var createDeparturePlace = new JButton("+");
		createDeparturePlace.setMinimumSize(new Dimension(40, 40));
		createDeparturePlace.addActionListener(this::onCreatePlace);
		var departureLayout = new SpringLayout();
		// Used to regroup the combo with the button
		var departurePanel = new JPanel(departureLayout);
		departurePanel.add(departurePlace);
		departurePanel.add(createDeparturePlace);
		SpringUtilities.makeCompactGrid(departurePanel, 1, 2, 0, 3, 3, 3);
		formPanel.add(departureLabel);
		formPanel.add(departurePanel);
		rows++;

		/* Arrival Place */
		var arrivalText = "Lieu d'arrivée";
		var arrivalLabel = new JLabel(arrivalText + " : ");
		arrivalPlace = new JComboBox<>(areas.toArray(new Place[0]));
		arrivalPlace.setMinimumSize(new Dimension(200, 20));
		arrivalPlace.setToolTipText(arrivalText);
		arrivalPlace.setRenderer(new ComboBoxRenderer.PlaceRenderer());
		var createArrivalPlace = new JButton("+");
		createArrivalPlace.setMinimumSize(new Dimension(40, 40));
		createArrivalPlace.addActionListener(this::onCreatePlace);
		var arrivalLayout = new SpringLayout();
		// Used to regroup the combo with the button
		var arrivalPanel = new JPanel(arrivalLayout);
		arrivalPanel.add(arrivalPlace);
		arrivalPanel.add(createArrivalPlace);
		SpringUtilities.makeCompactGrid(arrivalPanel, 1, 2, 0, 3, 3, 3);
		formPanel.add(arrivalLabel);
		formPanel.add(arrivalPanel);
		rows++;

		var events = loadEvents();

		/* Event */
		var eventText = "Événement";
		var eventLabel = new JLabel(eventText + " : ");
		relatedEvent = new JComboBox<>(events.toArray(new fr.ipb.soiree.data.Event[0]));
		relatedEvent.setMinimumSize(new Dimension(200, 20));
		relatedEvent.setToolTipText(eventText);
		relatedEvent.setRenderer(new ComboBoxRenderer.EventRenderer());
		var createRelatedEvent = new JButton("+");
		createRelatedEvent.setMinimumSize(new Dimension(40, 40));
		createRelatedEvent.addActionListener(this::onCreateEvent);
		var eventLayout = new SpringLayout();
		// Used to regroup the combo with the button
		var eventPanel = new JPanel(eventLayout);
		eventPanel.add(relatedEvent);
		eventPanel.add(createRelatedEvent);
		SpringUtilities.makeCompactGrid(eventPanel, 1, 2, 0, 3, 3, 3);
		formPanel.add(eventLabel);
		formPanel.add(eventPanel);
		rows++;

		SpringUtilities.makeCompactGrid(formPanel, rows, cols, 3, 3, 3, 3);

		/* Quit/Confirm */
		var actionPanel = new JPanel(new GridLayout(1, 2));
		var quit = new JButton("Annuler");
		quit.addActionListener(this::onQuit);
		var confirm = new JButton("Confirmer");
		confirm.addActionListener(this::onConfirmation);
		actionPanel.add(quit);
		actionPanel.add(confirm);

		borderPane.add(formPanel, BorderLayout.CENTER);
		borderPane.add(actionPanel, BorderLayout.SOUTH);
		add(borderPane);
		setTitle(title);
		setSize(600, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setVisible(true);
	}

	private void onQuit(ActionEvent e) {
		Logger.debug(this, "The user cancels the creation of a travel");
		dispose();
	}

	private void onConfirmation(ActionEvent e) {
		var startStamp = Timestamp.valueOf(start.datePicker.getDate().atStartOfDay()
				                                   .plusSeconds(
						                                   start.timePicker.getTime().toSecondOfDay()));

		try {
			var departurePlaceSelectedIndex = departurePlace.getSelectedIndex();
			var arrivalPlaceSelectedIndex = arrivalPlace.getSelectedIndex();
			var eventSelectedIndex = relatedEvent.getSelectedIndex();

			Travel.insert(db,
			              startStamp,
			              departurePlaceSelectedIndex >= 0 ? ((Place) departurePlace.getSelectedItem()) : null,
			              arrivalPlaceSelectedIndex >= 0 ? ((Place) arrivalPlace.getSelectedItem()) : null,
			              eventSelectedIndex > 0 ? ((fr.ipb.soiree.data.Event) relatedEvent.getSelectedItem()) : null
			);

			dispose();
		} catch (SQLException exception) {
			exception.printStackTrace();
			titleLabel.setText(exception.getLocalizedMessage());
		}
	}

	private void onCreatePlace(ActionEvent e) {
		new CreatePlaceScreen(db);
		Logger.trace(this, "The user has created a new Place.");
		departurePlace.removeAllItems();
		arrivalPlace.removeAllItems();
		for (Place p : loadPlaces()) {
			departurePlace.addItem(p);
			arrivalPlace.addItem(p);
		}
	}

	private void onCreateEvent(ActionEvent e) {
		new CreateEventScreen(db);
		Logger.trace(this, "The user has created a new Event.");
		relatedEvent.removeAllItems();
		for (fr.ipb.soiree.data.Event evt : loadEvents()) {
			relatedEvent.addItem(evt);
		}
	}

	private List<Place> loadPlaces() {
		try {
			return Place.all(db);
		} catch (SQLException e) {
			e.printStackTrace();
			titleLabel.setText("Impossible de charger les adresses");
			return Collections.emptyList();
		}
	}

	private List<fr.ipb.soiree.data.Event> loadEvents() {
		try {
			List<fr.ipb.soiree.data.Event> events = new ArrayList<>();
			events.add(FakeObjects.event);
			events.addAll(fr.ipb.soiree.data.Event.all(db));
			return events;
		} catch (SQLException e) {
			e.printStackTrace();
			titleLabel.setText("Impossible de charger les événements");
			return Collections.emptyList();
		}
	}
}

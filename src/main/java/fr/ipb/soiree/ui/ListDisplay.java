package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.utils.JLabelUtilities;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * A page displaying a list in a table.
 */
public class ListDisplay extends JFrame {

	public ListDisplay(String title, AbstractTableModel model) {
		Logger.trace(this, "Initializing the display list");

		var borderLayout = new BorderLayout();
		var pane = new JPanel(borderLayout);

		/* Title label */
		var titleLabel = new JLabel(title);
		titleLabel.setPreferredSize(new Dimension(200, 50));
		JLabelUtilities.resizeLabel(titleLabel, 24);
		titleLabel.setHorizontalAlignment(JLabel.CENTER);
		pane.add(titleLabel, BorderLayout.NORTH);

		// Table
		pane.add(new FormattedTable(model), BorderLayout.CENTER);

		/* Quit */
		var quit = new JButton("Quitter");
		quit.addActionListener(this::onQuit);
		pane.add(quit, BorderLayout.SOUTH);

		setSize(600, 600);

		getContentPane().add(pane);
		setVisible(true);
	}

	private void onQuit(ActionEvent e) {
		Logger.debug(this, "The list is being closed.");
		dispose();
	}
}

package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Area;
import fr.ipb.soiree.data.City;
import fr.ipb.soiree.data.Place;
import fr.ipb.soiree.sql.Database;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CreatePlaceScreen extends JDialog {

	private final Database db;

	private final JLabel title;
	private final JTextField address;
	private final JComboBox<Area> area;
	private final JComboBox<City> city;

	public CreatePlaceScreen(Database db) {
		Objects.requireNonNull(db);
		this.db = db;

		Logger.trace(this, "Initializing the place creation screen.");

		title = new JLabel("Nouveau lieu");
		title.setBounds(100, 50, 400, 40);
		add(title);

		address = new JTextField();
		address.setBounds(100, 100, 400, 40);
		address.setToolTipText("Adresse");
		add(address);

		var areas = loadAreas();
		area = new JComboBox<>(areas.toArray(new Area[0]));
		area.setBounds(100, 150, 400, 40);
		area.setToolTipText("Zone");
		area.setRenderer(new ComboBoxRenderer.AreaRenderer());
		add(area);

		var cities = loadCities();
		city = new JComboBox<>(cities.toArray(new City[0]));
		city.setBounds(100, 200, 400, 40);
		city.setToolTipText("Ville");
		city.setRenderer(new ComboBoxRenderer.CityRenderer());
		add(city);

		var confirm = new JButton("Confirmer");
		confirm.setBounds(100, 250, 400, 40);
		confirm.addActionListener(this::onConfirmation);
		add(confirm);

		setSize(600, 500);
		setLayout(null);
		setTitle("Nouvel événement");
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private List<Area> loadAreas() {
		try {
			return Area.all(db);
		} catch (SQLException e) {
			e.printStackTrace();
			title.setText("Impossible de charger les zones");
			return Collections.emptyList();
		}
	}

	private List<City> loadCities() {
		try {
			return City.all(db);
		} catch (SQLException e) {
			e.printStackTrace();
			title.setText("Impossible de charger les villes");
			return Collections.emptyList();
		}
	}

	private void onConfirmation(ActionEvent e) {
		if (address.getText().isBlank()) {
			title.setText("L'adresse est obligatoire");
			return;
		}

		try {
			Place.insert(db,
			             address.getText(),
			             ((Area) area.getSelectedItem()),
			             ((City) city.getSelectedItem())
			);

			dispose();
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText(exception.getLocalizedMessage());
		}
	}
}

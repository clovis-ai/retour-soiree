package fr.ipb.soiree.ui;

import fr.ipb.soiree.utils.JTableUtilities;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

public class FormattedTable extends JScrollPane {

	private final JTable table;

	public FormattedTable(AbstractTableModel model, TableColumnModel colModel, ListSelectionModel selectModel) {
		table = new JTable(model, colModel, selectModel);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		JTableUtilities.setCellsAlignment(table, SwingConstants.CENTER);
		JTableUtilities.setCellsMinimumWidth(table, 250);
		setViewportView(table);
		setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}

	public FormattedTable(AbstractTableModel model, TableColumnModel colModel) {
		this(model, colModel, null);
	}

	public FormattedTable(AbstractTableModel model) {
		this(model, null, null);
	}

	public JTable getTable() {
		return table;
	}
}

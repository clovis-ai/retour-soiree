package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Student;
import fr.ipb.soiree.data.Travel;
import fr.ipb.soiree.sql.Database;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * A user's main page, from where it can start all actions.
 */
public class UserDashboard extends JFrame {

	private final Database db;
	private final Student student;

	public UserDashboard(Database db, Student student) {
		Objects.requireNonNull(db);
		Objects.requireNonNull(student);
		this.db = db;
		this.student = student;
		Logger.trace(this, "Initializing the dashboard for: " + student.getEmail());

		var title = new JLabel("Bonjour, " + student.getFirstName() + " " + student.getLastName());
		title.setBounds(100, 50, 200, 40);
		add(title);

		var createEvent = new JButton("Nouvel événement");
		createEvent.setBounds(100, 100, 200, 40);
		createEvent.addActionListener(this::onEventCreation);
		add(createEvent);

		var createPlace = new JButton("Nouveau lieu");
		createPlace.setBounds(100, 150, 200, 40);
		createPlace.addActionListener(this::onPlaceCreation);
		add(createPlace);

		var research = new JButton("Rechercher un trajet");
		research.setBounds(100, 200, 200, 40);
		research.addActionListener(this::onTravelResearch);
		add(research);

		var createTravel = new JButton("Nouveau trajet");
		createTravel.setBounds(100, 250, 200, 40);
		createTravel.addActionListener(this::onTravelCreation);
		add(createTravel);

		var enrolledTravels = new JButton("Mes trajets");
		enrolledTravels.setBounds(100, 300, 200, 40);
		enrolledTravels.addActionListener(this::onTravelListEnrolled);
		add(enrolledTravels);

		var quit = new JButton("Déconnexion");
		quit.setBounds(100, 500, 200, 40);
		quit.addActionListener(this::onQuit);
		add(quit);

		setSize(600, 600);
		setLayout(null);
		setTitle("Utilisateur " + student.getFirstName());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void onPlaceCreation(ActionEvent e) {
		new CreatePlaceScreen(db);
	}

	private void onEventCreation(ActionEvent e) {
		new CreateEventScreen(db);
	}

	private void onTravelResearch(ActionEvent e) {
		Logger.debug(this,
		             "The user " + student.getEmail() + " wants to access to the travel research page.");
		new TravelResearchScreen(db, student);
	}

	private void onTravelCreation(ActionEvent e) {
		new CreateTravelScreen(db);
	}

	private void onTravelListEnrolled(ActionEvent e) {
		Logger.debug(this,
		             "The user " + student.getEmail() + " wants to access their enrolled travels page.");

		Supplier<List<Travel>> travels = () -> {
			try {
				var travelsDB = Student.allTravelsNotWithdrawn(db, student.getId());
				return new ArrayList<>(travelsDB);
			} catch (SQLException exception) {
				exception.printStackTrace();
				JOptionPane.showConfirmDialog(this, "Un problème est survenu " +
						"pendant la communication avec la base de données.");
				return new ArrayList<>();
			}
		};
		new TravelEnrollmentScreen(db, student, travels);
	}

	private void onQuit(ActionEvent e) {
		Logger.debug(this, "The user " + student.getEmail() + " wants to disconnect.");
		dispose();
	}

}

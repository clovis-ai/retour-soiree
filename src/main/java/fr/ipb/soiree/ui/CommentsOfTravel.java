package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Comment;
import fr.ipb.soiree.data.Enrollment;
import fr.ipb.soiree.data.Student;
import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.utils.JLabelUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.*;

public class CommentsOfTravel extends JDialog {

	private final Database db;
	private final Enrollment enrollment;
	private final JLabel title;
	private final JTextField newMessage;
	private final JComboBox<Object> grade;
	private final JList<Student> users;

	public CommentsOfTravel(Database db, Enrollment enrollment, Set<Comment> comments) {
		this.db = Objects.requireNonNull(db);
		this.enrollment = Objects.requireNonNull(enrollment);
		Logger.trace(this, "Initializing the comments list");

		var northPane = new JPanel(new GridLayout(3, 1));
		title = new JLabel("Commentaires échangés");
		JLabelUtilities.resizeLabel(title, 20);
		title.setHorizontalAlignment(JLabel.CENTER);
		northPane.add(title);

		newMessage = new JTextField();
		newMessage.setHorizontalAlignment(JLabel.CENTER);
		newMessage.setPreferredSize(new Dimension(300, 80));
		newMessage.setToolTipText("Commentaires");
		northPane.add(newMessage);

		grade = new JComboBox<>(new Object[]{"Pas de note", 0, 1, 2, 3, 4, 5});
		grade.setPreferredSize(new Dimension(300, 80));
		grade.setToolTipText("Note");
		northPane.add(grade);

		users = new JList<>(loadStudents().toArray(new Student[0]));
		users.setCellRenderer(new ComboBoxRenderer.StudentRenderer());
		users.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		users.setPreferredSize(new Dimension(300, 80));
		users.setToolTipText("Mentions");
		northPane.add(users);

		var send = new JButton("Envoyer");
		send.setHorizontalAlignment(JLabel.CENTER);
		send.setPreferredSize(new Dimension(200, 40));
		send.addActionListener(this::onSend);
		northPane.add(send);

		var model = new SoireeTableModel.CommentTableModel(new ArrayList<>(comments));
		FormattedTable table = new FormattedTable(model);
		var columns = table.getTable().getColumnModel();
		columns.getColumn(0).setMinWidth(100);
		columns.getColumn(1).setMinWidth(400);
		columns.getColumn(2).setMinWidth(150);

		setSize(600, 500);

		add(northPane, BorderLayout.NORTH);
		add(table, BorderLayout.CENTER);
		setVisible(true);
	}

	private List<Student> loadStudents() {
		try {
			return Student.all(db);
		} catch (SQLException throwables) {
			throwables.printStackTrace();
			title.setText("Impossible de charger les étudiants");
			return Collections.emptyList();
		}
	}

	private void onSend(ActionEvent e) {
		if (newMessage.getText().isBlank()) {
			title.setText("Le commentaire est obligatoire");
			return;
		}
		try {
			int rating = -1;
			if (grade.getSelectedItem() != "Pas de note" && grade.getSelectedItem() != null)
				rating = (int) grade.getSelectedItem();

			enrollment.addComment(db, newMessage.getText(),
			                      new Timestamp(System.currentTimeMillis()), rating,
			                      users.getSelectedValuesList());
			title.setText("Commentaire envoyé");
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText("Impossible d'envoyer le commentaire");
		}
	}
}

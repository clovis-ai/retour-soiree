package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Enrollment;
import fr.ipb.soiree.data.Message;
import fr.ipb.soiree.data.Student;
import fr.ipb.soiree.data.Travel;
import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.utils.JLabelUtilities;
import fr.ipb.soiree.utils.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class MessagesOfTravel extends JDialog {

	private final Database db;
	private final Travel travel;
	private final List<Pair<Message, Student>> messages;
	private final JLabel title;
	private final JTextField newMessage;
	private final Student user;

	private final FormattedTable table;

	public MessagesOfTravel(Database db, Travel travel, Student user, List<Pair<Message, Student>> messages) {
		this.db = Objects.requireNonNull(db);
		this.travel = Objects.requireNonNull(travel);
		this.messages = Objects.requireNonNull(messages);
		this.user = Objects.requireNonNull(user);
		Logger.trace(this, "Initializing the messages list");

		var northPane = new JPanel(new GridLayout(3, 1));
		title = new JLabel("Messages échangés");
		JLabelUtilities.resizeLabel(title, 20);
		title.setHorizontalAlignment(JLabel.CENTER);
		northPane.add(title);

		newMessage = new JTextField();
		newMessage.setHorizontalAlignment(JLabel.CENTER);
		newMessage.setPreferredSize(new Dimension(300, 80));
		newMessage.setToolTipText("Message");
		northPane.add(newMessage);

		var send = new JButton("Envoyer");
		send.setHorizontalAlignment(JLabel.CENTER);
		send.setPreferredSize(new Dimension(200, 40));
		send.addActionListener(this::onSend);
		northPane.add(send);

		var model = new SoireeTableModel.MessageTableModel(messages);
		table = new FormattedTable(model);
		var columns = table.getTable().getColumnModel();
		columns.getColumn(0).setMinWidth(100);
		columns.getColumn(1).setMinWidth(400);
		columns.getColumn(2).setMinWidth(150);

		setSize(600, 500);

		add(northPane, BorderLayout.NORTH);
		add(table, BorderLayout.CENTER);
		setVisible(true);
	}

	private void onSend(ActionEvent e) {
		if (newMessage.getText().isBlank()) {
			title.setText("Le message est obligatoire");
			return;
		}
		try {
			var enrollment = Enrollment.withStudentAndTravel(db, user, travel);
			if (enrollment.isPresent()) {
				Message.insert(db,
				               enrollment.get(),
				               newMessage.getText(),
				               new Timestamp(System.currentTimeMillis())
				);
				title.setText("Message envoyé");
			} else {
				title.setText(
						"Impossible d'envoyer le message : l'étudiant n'est pas inscrit au trajet");
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText("Impossible d'envoyer le message");
		}
	}
}

package fr.ipb.soiree.ui;

import com.github.lgooddatepicker.components.DateTimePicker;
import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Event;
import fr.ipb.soiree.data.Place;
import fr.ipb.soiree.sql.Database;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CreateEventScreen extends JDialog {

	private final Database db;

	private final JLabel title;
	private final JTextField description;
	private final DateTimePicker start;
	private final DateTimePicker end;
	private final JComboBox<Place> place;

	public CreateEventScreen(Database db) {
		Objects.requireNonNull(db);
		this.db = db;

		Logger.trace(this, "Initializing the event creation screen.");

		title = new JLabel("Nouvel événement");
		title.setBounds(100, 50, 400, 40);
		add(title);

		description = new JTextField();
		description.setBounds(100, 100, 400, 40);
		description.setToolTipText("Description");
		add(description);

		start = new DateTimePicker();
		start.setBounds(100, 150, 400, 40);
		start.datePicker.getSettings().setAllowEmptyDates(false);
		start.timePicker.getSettings().setAllowEmptyTimes(false);
		start.setToolTipText("Date de départ");
		add(start);

		end = new DateTimePicker();
		end.setBounds(100, 200, 400, 40);
		end.datePicker.getSettings().setAllowEmptyDates(false);
		end.timePicker.getSettings().setAllowEmptyTimes(false);
		end.setToolTipText("Date d'arrivée");
		add(end);

		var areas = loadPlaces();
		place = new JComboBox<>(areas.toArray(new Place[0]));
		place.setBounds(100, 250, 400, 40);
		place.setToolTipText("Lieu");
		place.setRenderer(new ComboBoxRenderer.PlaceRenderer());
		add(place);

		var createPlace = new JButton("+");
		createPlace.setBounds(500, 250, 40, 40);
		createPlace.addActionListener(this::onCreatePlace);
		add(createPlace);

		var confirm = new JButton("Confirmer");
		confirm.setBounds(100, 300, 400, 40);
		confirm.addActionListener(this::onConfirmation);
		add(confirm);

		setSize(600, 500);
		setLayout(null);
		setTitle("Nouvel événement");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setVisible(true);
	}

	private void onCreatePlace(ActionEvent e) {
		new CreatePlaceScreen(db);
		Logger.trace(this, "The user has created a new Place.");
		place.removeAllItems();
		for (Place p : loadPlaces())
			place.addItem(p);
	}

	private List<Place> loadPlaces() {
		try {
			return Place.all(db);
		} catch (SQLException e) {
			e.printStackTrace();
			title.setText("Impossible de charger les adresses");
			return Collections.emptyList();
		}
	}

	private void onConfirmation(ActionEvent e) {
		var startStamp = Timestamp.valueOf(start.datePicker.getDate().atStartOfDay()
				                                   .plusSeconds(
						                                   start.timePicker.getTime().toSecondOfDay()));
		var endStamp = Timestamp.valueOf(end.datePicker.getDate().atStartOfDay()
				                                 .plusSeconds(
						                                 end.timePicker.getTime().toSecondOfDay()));

		try {
			var placeIndex = place.getSelectedIndex();

			Event.insert(db,
			             description.getText(),
			             startStamp,
			             endStamp,
			             placeIndex >= 0 ? ((Place) place.getSelectedItem()) : null
			);

			dispose();
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText(exception.getLocalizedMessage());
		}
	}
}

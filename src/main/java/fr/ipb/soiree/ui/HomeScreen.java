package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.sql.Database;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * The app's entry point.
 */
public class HomeScreen extends JFrame {

	private final Database db = Database.connectAuto();

	public HomeScreen() {
		Logger.trace(this, "Initializing the home page.");

		var title = new JLabel("Retour de soirée");
		title.setBounds(100, 50, 200, 40);
		add(title);

		var login = new JButton("Connexion");
		login.setBounds(100, 100, 200, 40);
		login.addActionListener(this::onLogin);
		add(login);

		var newAccount = new JButton("Nouveau compte");
		newAccount.setBounds(100, 150, 200, 40);
		newAccount.addActionListener(this::onNewAccount);
		add(newAccount);

		var adminPage = new JButton("Accès administrateur");
		adminPage.setBounds(100, 200, 200, 40);
		adminPage.addActionListener(this::onAdminPageAccess);
		add(adminPage);

		var quit = new JButton("Quitter");
		quit.setBounds(100, 250, 200, 40);
		quit.addActionListener(this::onQuit);
		add(quit);

		setSize(400, 400);
		setLayout(null);
		setTitle("Page d'accueil");
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void onLogin(ActionEvent e) {
		Logger.debug(this, "The user wants to login.");

		new LoginScreen(db);
	}

	private void onNewAccount(ActionEvent e) {
		Logger.debug(this, "The user wants to create a new account.");

		new CreateUserScreen(db);
	}

	private void onAdminPageAccess(ActionEvent e) {
		Logger.debug(this, "The user wants to access the admin page.");

		new AdminDashboard(db);
	}

	private void onQuit(ActionEvent e) {
		Logger.info(this, "The user requested to quit.");
		dispose();
		System.exit(0);
	}

}

package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Event;
import fr.ipb.soiree.data.*;

import javax.swing.*;
import java.awt.*;

/**
 * Transforms objects into data that can be displayed in a {@link JComboBox}.
 * @param <T> The object that you want to display.
 */
abstract class ComboBoxRenderer<T> extends DefaultListCellRenderer {

	@Override
	public final Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		if (value != null) {
			T casted;
			try {
				//noinspection unchecked
				casted = (T) value;
				value = display(casted);
			} catch (ClassCastException e) {
				Logger.warn(this,"Could not cast value in ComboBox: " + value);
				e.printStackTrace();
				value = "?";
			}
		}

		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}

	protected abstract String display(T value);

	static class PlaceRenderer extends ComboBoxRenderer<Place> {
		@Override
		protected String display(Place place) {
			return place.getAddress() + ", " + place.getCity().getName();
		}
	}

	static class AreaRenderer extends ComboBoxRenderer<Area> {

		@Override
		protected String display(Area area) {
			return area.getName();
		}
	}

	static class StudentRenderer extends ComboBoxRenderer<Student> {

		@Override
		protected String display(Student value) {
			return value.getFullName();
		}
	}

	static class TravelRenderer extends ComboBoxRenderer<Travel> {

		@Override
		protected String display(Travel travel) {
			return "" + travel.getId();
		}
	}

	static class EventRenderer extends ComboBoxRenderer<Event> {

		@Override
		protected String display(Event event) {
			return event.getDescription() + (event.getPlace() == null ? "" : "," + event.getPlace().getArea().getName());
		}
	}

	static class CityRenderer extends ComboBoxRenderer<City> {
		@Override
		protected String display(City city) {
			return city.getName() + " (" + city.getZipCode() + ")";
		}
	}
}

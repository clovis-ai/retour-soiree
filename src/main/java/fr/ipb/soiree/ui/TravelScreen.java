package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Place;
import fr.ipb.soiree.data.Travel;
import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.utils.JLabelUtilities;
import fr.ipb.soiree.utils.SpringUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Objects;

public class TravelScreen extends JDialog {

	private final Database db;
	private final Travel travel;

	public TravelScreen(Database db, Travel travel) {
		this.db = Objects.requireNonNull(db);
		this.travel = Objects.requireNonNull(travel);

		setLayout(new BorderLayout());

		var title = "Description de trajet";
		var titleLabel = new JLabel(title);
		JLabelUtilities.resizeLabel(titleLabel, 24);
		titleLabel.setHorizontalAlignment(JLabel.CENTER);

		var centerPane = new JPanel(new SpringLayout());
		var rows = 0;

		var idLabel = new JLabel("ID :");
		var idValueLabel = new JLabel("" + travel.getId());
		centerPane.add(idLabel);
		centerPane.add(idValueLabel);
		rows++;

		var startLabel = new JLabel("Départ :");
		var startValueLabel = new JLabel(travel.getTime().toString());
		centerPane.add(startLabel);
		centerPane.add(startValueLabel);
		rows++;

		var departureLabel = new JLabel("Lieu de départ :");
		var departureValueLabel = new JLabel(placeDescription(travel.getDeparture()));
		centerPane.add(departureLabel);
		centerPane.add(departureValueLabel);
		rows++;

		var arrivalLabel = new JLabel("Lieu d'arrivée :");
		var arrivalValueLabel = new JLabel(placeDescription(travel.getArrival()));
		centerPane.add(arrivalLabel);
		centerPane.add(arrivalValueLabel);
		rows++;

		var eventLabel = new JLabel("Événement :");
		var event = travel.getEvent();
		String eventValue;
		if (event.isPresent())
			eventValue = travel.getEvent().toString();
		else
			eventValue = "Aucun événement";
		var eventValueLabel = new JLabel(eventValue);
		centerPane.add(eventLabel);
		centerPane.add(eventValueLabel);
		rows++;

		SpringUtilities.makeCompactGrid(centerPane, rows, 2, 5, 5, 5, 5);

		var quit = new JButton("Quitter");
		quit.addActionListener(this::onQuit);

		add(titleLabel, BorderLayout.NORTH);
		add(centerPane, BorderLayout.CENTER);
		add(quit, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}

	private void onQuit(ActionEvent e) {
		Logger.debug(this, "The user exits the description of travel " + travel.getId());
		dispose();
	}

	private String placeDescription(Place place) {
		return place.getAddress() + ", "
				+ place.getArea().getName() + ", "
				+ place.getCity().getZipCode() + " "
				+ place.getCity().getName();
	}

}

package fr.ipb.soiree.ui;

import com.github.lgooddatepicker.components.DateTimePicker;
import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Area;
import fr.ipb.soiree.data.Event;
import fr.ipb.soiree.data.Student;
import fr.ipb.soiree.data.Travel;
import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.utils.JLabelUtilities;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public class TravelResearchScreen extends JDialog {

	private final Database db;

	private final JLabel title;
	private final DateTimePicker start;
	private final JComboBox<Area> departure;
	private final JComboBox<Area> arrival;
	private final JComboBox<Event> event;
	private final Student user;

	public TravelResearchScreen(Database db, Student user) {

		this.db = Objects.requireNonNull(db);
		this.user = Objects.requireNonNull(user);
		Logger.trace(this, "Initializing the travel research screen");

		title = new JLabel("Rechercher un trajet");
		title.setBounds(200, 50, 400, 40);
		JLabelUtilities.resizeLabel(title, 24);
		add(title);

		start = new DateTimePicker();
		start.setBounds(100, 150, 400, 40);
		start.datePicker.getSettings().setAllowEmptyDates(true);
		start.timePicker.getSettings().setAllowEmptyTimes(true);
		start.setToolTipText("Date de départ");
		add(start);

		var allAreas = loadAreas();
		departure = new JComboBox<>(allAreas.toArray(new Area[0]));
		departure.setBounds(100, 200, 400, 40);
		departure.setToolTipText("Zone de départ");
		departure.setSelectedIndex(-1);
		departure.setRenderer(new ComboBoxRenderer.AreaRenderer());
		add(departure);

		arrival = new JComboBox<>(allAreas.toArray(new Area[0]));
		arrival.setBounds(100, 250, 400, 40);
		arrival.setToolTipText("Zone d'arrivée");
		arrival.setSelectedIndex(-1);
		arrival.setRenderer(new ComboBoxRenderer.AreaRenderer());
		add(arrival);

		var allEvents = loadEvents();
		event = new JComboBox<>(allEvents.toArray(new Event[0]));
		event.setBounds(100, 300, 400, 40);
		event.setToolTipText("Evénement");
		event.setSelectedIndex(-1);
		event.setRenderer(new ComboBoxRenderer.EventRenderer());
		add(event);

		var confirm = new JButton("Confirmer");
		confirm.setBounds(200, 500, 200, 40);
		confirm.addActionListener(this::onConfirmation);
		add(confirm);

		setSize(600, 600);
		setLayout(null);
		setVisible(true);
	}

	private void onConfirmation(ActionEvent e) {
		Logger.debug(this, "The user wants to research a travel.");
		var date = start.datePicker.getDate();
		var time = start.timePicker.getTime();
		var startStamp = date != null ?
				(time != null ?
						Timestamp.valueOf(
								date.atStartOfDay()
										.plusSeconds(
												time.toSecondOfDay()))
						: Timestamp.valueOf(date.atStartOfDay()))
				: null;

		Supplier<List<Travel>> lambda = () -> {
			try {
				return Travel.travelFilter(
						db,
						startStamp,
						departure.getSelectedIndex() >= 0 ? ((Area) departure.getSelectedItem()) : null,
						arrival.getSelectedIndex() >= 0 ? ((Area) arrival.getSelectedItem()) : null,
						event.getSelectedIndex() >= 0 ? ((Event) event.getSelectedItem()) : null);
			} catch (SQLException exception) {
				exception.printStackTrace();
				JOptionPane.showConfirmDialog(this, "Un problème est survenu " +
						"pendant la communication avec la base de données.");
				return new ArrayList<>();
			}
		};
		new TravelEnrollmentScreen(db, user, lambda);
	}

	private List<Area> loadAreas() {
		try {
			return Area.all(db);
		} catch (SQLException e) {
			e.printStackTrace();
			title.setText("Impossible de charger les zones");
			return Collections.emptyList();
		}
	}

	private List<Event> loadEvents() {
		try {
			return Event.all(db);
		} catch (SQLException e) {
			e.printStackTrace();
			title.setText("Impossible de charger les événements");
			return Collections.emptyList();
		}
	}
}

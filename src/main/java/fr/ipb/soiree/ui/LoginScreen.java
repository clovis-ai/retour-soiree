package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Student;
import fr.ipb.soiree.sql.Database;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;

/**
 * Allows the user to type in an email address to login.
 */
public class LoginScreen extends JDialog {

	private final Database db;

	private final JTextField username;
	private final JLabel title;

	public LoginScreen(Database db) {
		Objects.requireNonNull(db);
		this.db = db;

		Logger.trace(this, "Initializing the login screen.");

		title = new JLabel("Connexion");
		title.setBounds(100, 50, 200, 40);
		add(title);

		username = new JTextField();
		username.setBounds(100, 100, 200, 40);
		username.addActionListener(this::onConfirmation);
		username.setToolTipText("Adresse mail");
		add(username);

		var confirm = new JButton("Confirmer");
		confirm.setBounds(100, 150, 200, 40);
		confirm.addActionListener(this::onConfirmation);
		add(confirm);

		setSize(400, 300);
		setLayout(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void onConfirmation(ActionEvent actionEvent) {
		String email = username.getText();
		Logger.debug(this, "The user wants to login with account: " + email);

		try {
			var results = Student.withEmail(db, email);

			if (results.isEmpty()) {
				title.setText("Adresse inconnue");
			} else {
				var student = results.get();

				new UserDashboard(db, student);
				dispose();
			}

		} catch (SQLException e) {
			title.setText(e.getLocalizedMessage());
			Logger.warn(this, Arrays.toString(e.getStackTrace()));
		}
		// Nothing to do: the user should try again
	}

}

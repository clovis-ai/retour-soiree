package fr.ipb.soiree.ui;

import fr.ipb.soiree.data.*;
import fr.ipb.soiree.utils.Pair;

import javax.swing.table.AbstractTableModel;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SoireeTableModel<T> extends AbstractTableModel {

	private final String[] columnNames;
	private final List<T> data;
	private final Function<T, Object[]> mapper;

	public SoireeTableModel(String[] columnNames, List<T> data, Function<T, Object[]> mapper) {
		this.columnNames = Objects.requireNonNull(columnNames);
		this.data = Objects.requireNonNull(data);
		this.mapper = Objects.requireNonNull(mapper);
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {
		if (row >= data.size() || row < 0)
			throw new IndexOutOfBoundsException("Given row out of bounds: " + row);
		if (col >= columnNames.length || col < 0) {
			throw new IndexOutOfBoundsException("Given column out of bounds: " + col);
		}
		return mapper.apply(data.get(row))[col];
	}

	@Override
	public Class<?> getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public static class PlaceTableModel extends SoireeTableModel<Place> {

		public PlaceTableModel(List<Place> data) {
			super(new String[]{"ID", "Adresse", "Zone", "Ville"},
			      data,
			      mapper);
		}

		private static final Function<Place, Object[]> mapper = (Place p) ->
				new Object[]{p.getId(), p.getAddress(), p.getArea().getName(), p.getCity().getName()};

	}

	public static class MessageTableModel extends SoireeTableModel<Pair<Message, Student>> {

		public MessageTableModel(List<Pair<Message, Student>> data) {
			super(new String[]{"De", "Message", "Date"}, data, mapper);
		}

		private static final Function<Pair<Message, Student>, Object[]> mapper = (Pair<Message, Student> p) ->
				new Object[]{p.right.getFirstName() + ' ' + p.right.getLastName(), p.left.getContent(), p.left.getTime()};

	}

	public static class CommentTableModel extends SoireeTableModel<Comment> {

		public CommentTableModel(List<Comment> comments) {
			super(new String[]{"De", "Message", "Date", "Note", "Mentions"}, comments, comment -> {
				var author = comment.getEnrollment().getStudent();
				return new Object[]{
						author.getFullName(),
						comment.getContent(),
						comment.getTime(),
						comment.getRating().stream()
								.mapToObj(String::valueOf)
								.findFirst()
								.orElse("Ø"),
						comment.getMentions().stream()
								.map(Student::getFullName)
								.collect(Collectors.joining(", "))
				};
			});
		}
	}

	public static class TravelTableModel extends SoireeTableModel<Travel> {

		public TravelTableModel(List<Travel> data) {
			super(new String[]{"ID", "Heure", "Départ", "Arrivée", "Événement"},
			      data,
			      mapper);
		}

		private static final Function<Travel, Object[]> mapper = (Travel t) ->
				new Object[]{
						t.getId(),
						t.getTime(),
						t.getDeparture().getAddress() + ", " + t.getDeparture().getCity().getName(),
						t.getArrival().getAddress() + ", " + t.getArrival().getCity().getName(),
						t.getEvent().isPresent() ? t.getEvent().get().getDescription() : "Pas d'événement"};
	}

	public static class TravelCountingStudentTableModel extends SoireeTableModel<Pair<Travel, Integer>> {

		public TravelCountingStudentTableModel(List<Pair<Travel, Integer>> data) {
			super(new String[]{"ID", "Heure", "Départ", "Arrivée", "Événement", "Nombre d'étudiants"},
			      data,
			      mapper);
		}

		private static final Function<Pair<Travel, Integer>, Object[]> mapper = (Pair<Travel, Integer> pair) ->
				new Object[]{
						pair.left.getId(),
						pair.left.getTime(),
						pair.left.getDeparture().getAddress() + ", " + pair.left.getDeparture().getCity().getName(),
						pair.left.getArrival().getAddress() + ", " + pair.left.getArrival().getCity().getName(),
						pair.left.getEvent().isPresent() ? pair.left.getEvent().get().getDescription() : "Pas d'événement",
						pair.right};
	}

	public static class StudentTableModel extends SoireeTableModel<Student> {

		public StudentTableModel(List<Student> data) {
			super(new String[]{"ID", "Prénom", "Nom", "Téléphone", "Email"},
			      data,
			      mapper);
		}

		private static final Function<Student, Object[]> mapper = (Student s) ->
				new Object[]{
						s.getId(),
						s.getFirstName(),
						s.getLastName(),
						s.getPhoneNumber(),
						s.getEmail()
				};
	}

	public static class StudentCountingTravelsAndAvgRatingTableModel extends SoireeTableModel<Pair<Student, Pair<Integer, Float>>> {

		public StudentCountingTravelsAndAvgRatingTableModel(List<Pair<Student, Pair<Integer, Float>>> data) {
			super(new String[]{"ID", "Prénom", "Nom", "Téléphone", "Email", "Nombre de trajets", "Évaluation moyenne"},
			      data,
			      mapper);
		}

		private static final Function<Pair<Student, Pair<Integer, Float>>, Object[]> mapper = (Pair<Student, Pair<Integer, Float>> pair) ->
				new Object[]{
						pair.left.getId(),
						pair.left.getFirstName(),
						pair.left.getLastName(),
						pair.left.getPhoneNumber(),
						pair.left.getEmail(),
						pair.right.left,
						pair.right.right == -1 ? "Aucune" : pair.right.right
				};
	}

	public static class AreaTableModel extends SoireeTableModel<Area> {

		public AreaTableModel(List<Area> data) {
			super(new String[]{"ID", "Nom"},
			      data,
			      mapper);
		}

		private static final Function<Area, Object[]> mapper = (Area a) ->
				new Object[]{
						a.getId(),
						a.getName()
				};
	}

	public static class AreaCountingPlaceTableModel extends SoireeTableModel<Pair<Area, Integer>> {

		public AreaCountingPlaceTableModel(List<Pair<Area, Integer>> data) {
			super(new String[]{"ID", "Nom", "Nombre de lieux"},
			      data,
			      mapper);
		}

		private static final Function<Pair<Area, Integer>, Object[]> mapper = (Pair<Area, Integer> pair) ->
				new Object[]{
						pair.left.getId(),
						pair.left.getName(),
						pair.right};
	}

	public static class AreaMostPopularPlaceTableModel extends SoireeTableModel<Pair<Area, Set<Place>>> {

		public AreaMostPopularPlaceTableModel(List<Pair<Area, Set<Place>>> data) {
			super(new String[]{"ID", "Nom", "Lieu le plus populaire"},
			      data,
			      mapper);
		}

		private static final Function<Pair<Area, Set<Place>>, Object[]> mapper = (Pair<Area, Set<Place>> pair) ->
				new Object[]{
						pair.left.getId(),
						pair.left.getName(),
						pair.right.iterator().next().getAddress()};
	}

	public static class CityTableModel extends SoireeTableModel<City> {

		public CityTableModel(List<City> data) {
			super(new String[]{"ID", "Code postal", "Nom"},
			      data,
			      mapper);
		}

		private static final Function<City, Object[]> mapper = (City c) ->
				new Object[]{
						c.getId(),
						c.getZipCode(),
						c.getName()
				};
	}

	public static class EventTableModel extends SoireeTableModel<Event> {

		public EventTableModel(List<Event> data) {
			super(new String[]{"ID", "Description", "Date début", "Date fin", "Lieu"},
			      data,
			      mapper);
		}

		private static final Function<Event, Object[]> mapper = (Event c) ->
				new Object[]{
						c.getId(),
						c.getDescription(),
						c.getStart(),
						c.getEnd(),
						c.getPlace().getAddress() + " - " + c.getPlace().getCity().getName()
				};
	}

}

package fr.ipb.soiree.ui;

import com.jcabi.log.Logger;
import fr.ipb.soiree.data.Student;
import fr.ipb.soiree.sql.Database;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.Objects;

public class CreateUserScreen extends JDialog {

	private final Database db;

	private final JLabel title;
	private final JTextField email;
	private final JTextField firstName;
	private final JTextField lastName;
	private final JTextField phoneNumber;

	public CreateUserScreen(Database db) {
		Objects.requireNonNull(db);
		this.db = db;

		Logger.trace(this, "Initializing the login screen.");

		title = new JLabel("Nouveau compte");
		title.setBounds(100, 50, 200, 40);
		add(title);

		email = new JTextField();
		email.setBounds(100, 100, 200, 40);
		email.setToolTipText("Adresse mail");
		add(email);

		firstName = new JTextField();
		firstName.setBounds(100, 150, 200, 40);
		firstName.setToolTipText("Prénom");
		add(firstName);

		lastName = new JTextField();
		lastName.setBounds(100, 200, 200, 40);
		lastName.setToolTipText("Nom de famille");
		add(lastName);

		phoneNumber = new JTextField();
		phoneNumber.setBounds(100, 250, 200, 40);
		phoneNumber.setToolTipText("Numéro de téléphone");
		add(phoneNumber);

		var confirm = new JButton("Confirmer");
		confirm.setBounds(100, 300, 200, 40);
		confirm.addActionListener(this::onConfirmation);
		add(confirm);

		setSize(400, 500);
		setLayout(null);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private void onConfirmation(ActionEvent e) {
		if (!email.getText().contains("@")) {
			title.setText("Adresse mail invalide");
			email.requestFocusInWindow();

			return;
		}

		if (firstName.getText().isBlank()) {
			title.setText("Prénom manquant");
			firstName.requestFocusInWindow();

			return;
		}

		if (lastName.getText().isBlank()) {
			title.setText("Nom de famille manquant");
			lastName.requestFocusInWindow();

			return;
		}

		String phoneNumber = this.phoneNumber.getText().isBlank() ? null : this.phoneNumber.getText();

		Student student;
		try {
			student = Student.insert(db,
			                         firstName.getText(),
			                         lastName.getText(),
			                         phoneNumber,
			                         email.getText());

			new UserDashboard(db, student);
			dispose();
		} catch (SQLException exception) {
			exception.printStackTrace();
			title.setText(exception.getLocalizedMessage());
		}
	}

}

package fr.ipb.soiree;

import com.jcabi.log.Logger;
import fr.ipb.soiree.ui.HomeScreen;

public class App {
	public static void main(String[] args) {
		Logger.info(App.class, "Starting…");

		new HomeScreen();

		Logger.info(App.class, "Starting up done.");
		System.out.println();
	}
}

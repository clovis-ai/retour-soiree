package fr.ipb.soiree.data;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * FakeObjects exists solely for the UI, whenever an element can be null in a UI object like
 * JComboBox. Each object is supposed to be prevented from being added in the database.
 */
public class FakeObjects {

	public static final Event event = new Event(-1,
	                                            "Aucun",
	                                            Timestamp.from(Instant.now()),
	                                            Timestamp.from(Instant.now()),
	                                            null);

}

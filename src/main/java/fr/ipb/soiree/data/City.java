package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.utils.InvalidDataException;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class City {

	private final long id;
	private final int zipCode;
	private final String name;

	public City(long id, int zipCode, String name) {
		Objects.requireNonNull(name);
		this.id = id;
		this.zipCode = zipCode;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public int getZipCode() {
		return zipCode;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof City)) return false;
		City city = (City) o;
		return id == city.id &&
				zipCode == city.zipCode &&
				name.equals(city.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, zipCode, name);
	}

	@Override
	public String toString() {
		return "City{" +
				"id=" + id +
				", zipCode=" + zipCode +
				", name='" + name + '\'' +
				'}';
	}

	private static final Database.SqlMapper<City> sqlMapper = results -> new City(
			results.getLong(1),
			results.getInt(2),
			results.getString(3)
	);

	public static Optional<City> withId(Database db, long id) throws SQLException {
		return db.selectOne(Request.CITY_ID, statement -> statement.setLong(1, id),
		                    sqlMapper);
	}

	public static List<City> all(Database db) throws SQLException {
		return db.selectAll(Request.CITY_ALL, sqlMapper);
	}

	public static City insert(Database db, int zipCode, String name) throws SQLException {
		Objects.requireNonNull(name);
		if (zipCode <= 0)
			throw new InvalidDataException("Tried to insert a city with an invalid zip code");
		if (name.isBlank())
			throw new InvalidDataException("Tried to insert a city with a blank name");

		long lastId;
		try (var transaction = db.start()) {

			var result = transaction.update(Request.CITY_INSERT, statement -> {
				statement.setLong(1, zipCode);
				statement.setString(2, name);
			});

			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the city: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new City(lastId, zipCode, name);
	}
}

package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.sql.Transaction;
import fr.ipb.soiree.utils.InvalidDataException;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Place {

	private final long id;
	private final String address;
	private final Area area;
	private final City city;

	public Place(long id, String address, Area area, City city) {
		this.id = id;
		this.address = Objects.requireNonNull(address);
		this.area = Objects.requireNonNull(area);
		this.city = Objects.requireNonNull(city);
	}

	public long getId() {
		return id;
	}

	public String getAddress() {
		return address;
	}

	public Area getArea() {
		return area;
	}

	public City getCity() {
		return city;
	}

	@Override
	public String toString() {
		return "Place{" +
				"id=" + id +
				", address='" + address + '\'' +
				", area=" + area +
				", city=" + city +
				'}';
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Place place = (Place) obj;
		return id == place.id &&
				address.equals(place.address) &&
				area.equals(place.area) &&
				city.equals(place.city);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, address, area, city);
	}

	private static final Database.SqlMapper<Place> placeMapper = cols -> new Place(
			cols.getLong(1),
			cols.getString(2),
			new Area(
					cols.getLong(3),
					cols.getString(4)),
			new City(
					cols.getLong(5),
					cols.getInt(6),
					cols.getString(7))
	);

	/**
	 * Returns the place with the given id.
	 *
	 * @param db The database to connect to.
	 * @param id The place's id.
	 * @return An optional populated with a place if found, empty otherwise.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Optional<Place> withID(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);

		Transaction.SetParameters params = statement -> statement.setLong(1, id);
		return db.selectOne(Request.PLACE_WITH_ID, params, placeMapper);
	}

	public static List<Place> all(Database db) throws SQLException {
		return db.selectAll(Request.PLACE_ALL, placeMapper);
	}

	public static Place insert(Database db, String address, Area area, City city) throws SQLException {
		Objects.requireNonNull(address);
		Objects.requireNonNull(area);
		Objects.requireNonNull(city);

		long lastId;
		try (var transaction = db.start()) {
			var result = transaction.update(Request.PLACE_INSERT, statement -> {
				statement.setString(1, address);
				statement.setLong(2, area.getId());
				statement.setLong(3, city.getId());
			});

			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the place: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new Place(lastId, address, area, city);
	}
}

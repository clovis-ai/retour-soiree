package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.sql.Transaction;
import fr.ipb.soiree.utils.InvalidDataException;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

final public class Enrollment {

	private final long id;
	private final boolean withdrawn;
	private final Student student;
	private final Travel travel;

	Enrollment(long id, boolean withdrawn, Student student, Travel travel) {
		this.id = id;
		this.withdrawn = withdrawn;
		this.student = Objects.requireNonNull(student);
		this.travel = Objects.requireNonNull(travel);
	}

	public long getId() {
		return id;
	}

	public boolean isWithdrawn() {
		return withdrawn;
	}

	public Student getStudent() {
		return student;
	}

	public Travel getTravel() {
		return travel;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Enrollment)) return false;
		Enrollment that = (Enrollment) o;
		return id == that.id && withdrawn == that.withdrawn && student.equals(
				that.student) && travel.equals(that.travel);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, withdrawn, student, travel);
	}

	@Override
	public String toString() {
		return "Enrollment{" +
				"id=" + id +
				", withdrawn=" + withdrawn +
				", student=" + student +
				", travel=" + travel +
				'}';
	}

	private static Database.SqlMapper<Enrollment> enrollmentMapperFromDB(Database db) {
		return (cols) -> {
			long studentID = cols.getLong(3);
			long travelID = cols.getLong(4);
			return new Enrollment(
					cols.getLong(1),
					cols.getBoolean(2),
					Student.withId(db, studentID).orElseThrow(
							() -> new SQLException("Used Student.ID does not exist: " + studentID)),
					Travel.withID(db, travelID).orElseThrow(
							() -> new SQLException("Used Travel.ID does not exist: " + travelID))
			);
		};
	}

	/**
	 * Adds a comment on this travel.
	 *
	 * @param db       The database
	 * @param content  The text they want to type
	 * @param time     When is the comment posted?
	 * @param rating   The grade given to the mentioned users: -1 for no grade, 0-5 to give a grade
	 * @param mentions The users you want to mention
	 * @return A new comment
	 * @throws SQLException If anything went wrong with the database
	 */
	public Comment addComment(Database db, String content, Timestamp time, int rating, List<Student> mentions) throws SQLException {
		Objects.requireNonNull(content);
		Objects.requireNonNull(time);
		Objects.requireNonNull(mentions);
		if (rating < -1 || rating > 5)
			throw new IllegalArgumentException("The rating should be between -1 and 5: " + rating);
		if (content.isBlank()) throw new IllegalArgumentException("Cannot write an empty message");

		final long commentId;
		try (var transaction = db.start()) {

			// Insert the message
			final int messageInsertion = transaction.update(Request.MESSAGE_INSERT, statement -> {
				statement.setLong(1, getId());
				statement.setString(2, content);
				statement.setTimestamp(3, time);
			});

			if (messageInsertion != 1)
				throw new IllegalStateException(
						"Could not insert the message: " + messageInsertion + " line·s would be modified (aborted)");

			final long messageId = transaction.lastId();

			// Insert the comment
			final int commentInsertion = transaction.update(Request.COMMENTS_INSERT, statement -> {
				statement.setLong(1, messageId);
				if (rating == -1) statement.setNull(2, Types.INTEGER);
				else statement.setLong(2, rating);
			});

			if (commentInsertion != 1)
				throw new IllegalStateException(
						"Could not insert the comment: " + commentInsertion + " lines·s would be modified (aborted)");

			commentId = transaction.lastId();

			// Insert mentions
			for (Student student : mentions) {
				final int mentionInsertion = transaction.update(Request.COMMENTMENTIONS_INSERT,
				                                                statement -> {
					                                                statement.setLong(1, commentId);
					                                                statement.setLong(2,
					                                                                  student.getId());
				                                                });

				if (mentionInsertion != 1)
					throw new IllegalStateException(
							"Could not insert the mention of " + student.getEmail() + ": " + mentionInsertion + " line·s would be modified");
			}

			// If everything went well
			transaction.commit();
		}

		return new Comment(commentId, this, content, time, rating);
	}

	public static Optional<Enrollment> withStudentAndTravel(Database db, Student student, Travel travel) throws SQLException {
		Objects.requireNonNull(db);
		Transaction.SetParameters params = statement -> {
			statement.setLong(1, student.getId());
			statement.setLong(2, travel.getId());
		};
		return db.selectOne(Request.ENROLLMENT_WITH_STUDENT_AND_TRAVEL, params,
		                    enrollmentMapperFromDB(db));
	}

	public static Enrollment enroll(Database db, Student student, Travel travel) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(student);
		Objects.requireNonNull(travel);

		long enrollmentId;
		var enrollment = withStudentAndTravel(db, student, travel);
		if (enrollment.isPresent()) {
			enrollmentId = enrollment.get().getId();
			unwithdraw(db, enrollmentId);
		} else {
			var enrolled = insert(db, false, student, travel);
			enrollmentId = enrolled.getId();
		}

		return new Enrollment(enrollmentId, false, student, travel);
	}

	public static Optional<Enrollment> withdraw(Database db, Student student, Travel travel) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(student);
		Objects.requireNonNull(travel);

		var enrollment = withStudentAndTravel(db, student, travel);

		return enrollment.map((e) -> {
			var id = e.getId();
			try {
				withdraw(db, id);
			} catch (SQLException exception) {
				throw new RuntimeException(exception);
			}
			return new Enrollment(id, true, student, travel);
		});
	}

	private static Enrollment insert(Database db, boolean withdrawn, Student student, Travel travel) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(student);
		Objects.requireNonNull(travel);

		long lastId;
		try (var transaction = db.start()) {

			var result = transaction.update(Request.ENROLLMENT_INSERT, statement -> {
				statement.setBoolean(1, withdrawn);
				statement.setLong(2, student.getId());
				statement.setLong(3, travel.getId());
			});
			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the enrollment: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new Enrollment(lastId, withdrawn, student, travel);
	}

	private static void unwithdraw(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);

		try (var transaction = db.start()) {

			int result = transaction.update(Request.ENROLLMENT_UNWITHDRAW, statement ->
					statement.setLong(1, id));

			if (result != 1)
				throw new InvalidDataException(
						"Could not unwithdraw the enrollment: " + result + " line·s would be modified +(aborted)");

			transaction.commit();
		}
	}

	private static void withdraw(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);

		try (var transaction = db.start()) {

			int result = transaction.update(Request.ENROLLMENT_WITHDRAW, statement ->
					statement.setLong(1, id));

			if (result != 1)
				throw new InvalidDataException(
						"Could not withdraw the enrollment: " + result + " line·s would be modified (aborted)");
			transaction.commit();
		}
	}

	public static Optional<Enrollment> withId(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);
		Transaction.SetParameters params = statement -> statement.setLong(1, id);
		return db.selectOne(Request.ENROLLMENT_WITH_ID, params,
		                    enrollmentMapperFromDB(db));
	}
}

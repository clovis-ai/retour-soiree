package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.sql.Transaction;
import fr.ipb.soiree.utils.InvalidDataException;
import fr.ipb.soiree.utils.Pair;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.util.*;

final public class Travel {

	private final long id;
	private final Timestamp time;
	private final Place departure;
	private final Place arrival;
	private final Event event;

	Travel(long id, Timestamp time, Place departure, Place arrival, Event event) {
		Objects.requireNonNull(time);
		Objects.requireNonNull(departure);
		Objects.requireNonNull(arrival);
		this.id = id;
		this.time = time;
		this.departure = departure;
		this.arrival = arrival;
		this.event = event;
	}

	public long getId() {
		return id;
	}

	public Timestamp getTime() {
		return time;
	}

	public Place getDeparture() {
		return departure;
	}

	public Place getArrival() {
		return arrival;
	}

	public Optional<Event> getEvent() {
		return (event == null) ? Optional.empty() : Optional.of(event);
	}

	@Override
	public String toString() {
		return "Travel{" +
				"id=" + id +
				", time=" + time +
				", departure=" + departure +
				", arrival=" + arrival +
				", event=" + event +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Travel travel = (Travel) o;
		return id == travel.id &&
				time.equals(travel.time) &&
				departure.equals(travel.departure) &&
				arrival.equals(travel.arrival) &&
				Objects.equals(event, travel.event);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, time, departure, arrival, event);
	}

	public static Database.SqlMapper<Travel> travelMapperFromDB(Database db) {
		return (cols) -> {
			long placeID1 = cols.getLong(3);
			long placeID2 = cols.getLong(4);
			long eventID = cols.getLong(5);
			return new Travel(
					cols.getLong(1),
					cols.getTimestamp(2),
					Place.withID(db, placeID1).orElseThrow(
							() -> new SQLException("Used Place.ID does not exist: " + placeID1)),
					Place.withID(db, placeID2).orElseThrow(
							() -> new SQLException("Used Place.ID does not exist: " + placeID2)),
					eventID == 0 ? null : Event.withID(db, eventID).orElseThrow(
							() -> new SQLException("Used Event.ID does not exist: " + eventID))
			);
		};
	}

	public static Set<Travel> all(Database db) throws SQLException {
		Objects.requireNonNull(db);
		Database.SqlMapper<Travel> travelMapper = travelMapperFromDB(db);
		return new HashSet<>(db.selectAll(Request.TRAVEL_ALL, travelMapper));
	}

	/**
	 * Returns the travels with given departure and arrival areas and date.
	 *
	 * @param db        The database to connect to.
	 * @param departure The travel's departure area.
	 * @param arrival   The travel's arrival area.
	 * @param date      The travel's date.
	 * @return A set of travels.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Set<Travel> withAreasAndTime(Database db, Area departure, Area arrival, Date date) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(departure);
		Objects.requireNonNull(arrival);
		Objects.requireNonNull(date);

		Database.SqlMapper<Travel> travelMapper = travelMapperFromDB(db);

		Transaction.SetParameters params = statement -> {
			statement.setLong(1, departure.getId());
			statement.setLong(2, arrival.getId());
			statement.setDate(3, date);
		};

		return new HashSet<>(
				db.selectAll(Request.TRAVELS_WITH_AREAS_AND_TIME, params, travelMapper));
	}

	/**
	 * Returns the travels that aren't linked to events.
	 *
	 * @param db The database to connect to.
	 * @return A set of travels.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Set<Travel> withoutEvent(Database db) throws SQLException {
		Objects.requireNonNull(db);

		Database.SqlMapper<Travel> travelMapper = travelMapperFromDB(db);
		return new HashSet<>(db.selectAll(Request.TRAVELS_WITHOUT_EVENT, travelMapper));
	}

	private static Database.SqlMapper<Pair<Travel, Integer>> travelIntMapperFromDB(Database db) {
		return cols -> {
			long placeID1 = cols.getLong(3);
			long placeID2 = cols.getLong(4);
			long eventID = cols.getLong(5);
			return new Pair<>(
					new Travel(
							cols.getLong(1),
							cols.getTimestamp(2),
							Place.withID(db, placeID1).orElseThrow(
									() -> new SQLException(
											"Used Place.ID does not exist: " + placeID1)),
							Place.withID(db, placeID2).orElseThrow(
									() -> new SQLException(
											"Used Place.ID does not exist: " + placeID2)),
							eventID == 0 ? null : Event.withID(db, eventID).orElseThrow(
									() -> new SQLException(
											"Used Event.ID does not exist: " + eventID))),
					cols.getInt(6)
			);
		};
	}

	public static Map<Travel, Integer> studentsByTravel(Database db) throws SQLException {
		Database.SqlMapper<Pair<Travel, Integer>> travelIntMapper = travelIntMapperFromDB(db);
		Map<Travel, Integer> m = new HashMap<>();
		for (Pair<Travel, Integer> pair : db.selectAll(
				Request.STUDENTS_BY_TRAVEL, travelIntMapper)) {
			m.put(pair.left, pair.right);
		}
		return m;
	}

	public static Optional<Travel> withID(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);

		Transaction.SetParameters params = statement -> statement.setLong(1, id);

		return db.selectOne(Request.TRAVELS_WITH_ID, params, travelMapperFromDB(db));
	}

	public static Set<Travel> withEvent(Database db, Event event) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(event);
		Database.SqlMapper<Travel> travelMapper = travelMapperFromDB(db);
		return new HashSet<>(db.selectAll(Request.TRAVELS_WITH_EVENT,
		                                  statement -> statement.setLong(1, event.getId()),
		                                  travelMapper));
	}

	public static Set<Travel> withDepartureArea(Database db, Area area) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(area);
		Database.SqlMapper<Travel> travelMapper = travelMapperFromDB(db);
		return new HashSet<>(db.selectAll(Request.TRAVELS_WITH_DEP_AREA,
		                                  statement -> statement.setLong(1, area.getId()),
		                                  travelMapper));
	}

	public static Set<Travel> withArrivalArea(Database db, Area area) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(area);
		Database.SqlMapper<Travel> travelMapper = travelMapperFromDB(db);
		return new HashSet<>(db.selectAll(Request.TRAVELS_WITH_ARR_AREA,
		                                  statement -> statement.setLong(1, area.getId()),
		                                  travelMapper));
	}

	public static Set<Travel> withDate(Database db, Timestamp time) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(time);
		Database.SqlMapper<Travel> travelMapper = travelMapperFromDB(db);
		return new HashSet<>(db.selectAll(Request.TRAVELS_WITH_DATE,
		                                  statement -> statement.setTimestamp(1, time),
		                                  travelMapper));
	}

	public static List<Travel> travelFilter(Database db, Timestamp time, Area departure, Area arrival, Event event) throws SQLException {
		Objects.requireNonNull(db);
		Set<Travel> travels;
		travels = Travel.all(db);
		if (time != null) {
			travels.retainAll(Travel.withDate(db, time));
		}
		if (departure != null) {
			travels.retainAll(Travel.withDepartureArea(db, departure));
		}
		if (arrival != null) {
			travels.retainAll(Travel.withArrivalArea(db, arrival));
		}
		if (event != null) {
			travels.retainAll(Travel.withEvent(db, event));
		}
		return new ArrayList<>(travels);
	}

	/**
	 * Inserts given travel into given database using given elements.
	 *
	 * @param db        The database to insert to.
	 * @param time      The departure date.
	 * @param departure The departure place.
	 * @param arrival   The arrival place.
	 * @param event     The related event.
	 * @return The inserted travel with the actual id managed by the database.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Travel insert(Database db, Timestamp time, Place departure, Place arrival, Event event) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(time);
		Objects.requireNonNull(departure);
		Objects.requireNonNull(arrival);
		var now = Timestamp.from(Instant.now());
		if (time.before(now))
			throw new InvalidDataException("Tried to insert an outdated travel.");

		Transaction.SetParameters paramsTravel = statement -> {
			statement.setTimestamp(1, time);
			statement.setLong(2, departure.getId());
			statement.setLong(3, arrival.getId());
			if (event == null)
				statement.setNull(4, Types.BIGINT);
			else
				statement.setLong(4, event.getId());
		};

		long lastId;
		try (var transaction = db.start()) {

			var result = transaction.update(Request.TRAVELS_INSERT, paramsTravel);

			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the travel: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new Travel(lastId, time, departure, arrival, event);
	}
}

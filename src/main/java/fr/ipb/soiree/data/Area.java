package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Database.SqlMapper;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.utils.InvalidDataException;
import fr.ipb.soiree.utils.Pair;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Area {

	private final long id;
	private final String name;

	public Area(long id, String name) {
		Objects.requireNonNull(name);
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Area)) return false;
		Area area = (Area) o;
		return id == area.id &&
				name.equals(area.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public String toString() {
		return "Area{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}

	private static final SqlMapper<Pair<Area, Integer>> areaIntMapper = cols -> new Pair<>(
			new Area(
					cols.getLong(1),
					cols.getString(2)),
			cols.getInt(3)
	);
	private static final SqlMapper<Area> areaSqlMapper = results -> new Area(
			results.getLong(1),
			results.getString(2)
	);

	public static Map<Area, Integer> allAreasWithPLaces(Database db) throws SQLException {
		Map<Area, Integer> m = new HashMap<>();
		for (Pair<Area, Integer> pair : db.selectAll(
				Request.ALL_AREAS_WITH_PLACES, areaIntMapper)) {
			m.put(pair.left, pair.right);
		}
		return m;
	}

	public static Map<Area, Set<Place>> mostPopularPlaces(Database db) throws SQLException {
		Map<Area, Set<Place>> m = new HashMap<>();
		for (Pair<Area, Place> pair : db.selectAll(
				Request.MOST_POPULAR_PLACES, Area::areaPlaceMapper)) {
			if (m.containsKey(pair.left)) {
				m.get(pair.left).add(pair.right);
			} else {
				var set = new HashSet<Place>();
				set.add(pair.right);
				m.put(pair.left, set);
			}
		}
		return m;
	}

	private static Pair<Area, Place> areaPlaceMapper(ResultSet cols) throws SQLException {
		Area area = new Area(cols.getLong(1),
		                     cols.getString(2));
		return new Pair<>(
				area,
				new Place(
						cols.getLong(3),
						cols.getString(4),
						area,
						new City(cols.getLong(7), cols.getInt(8), cols.getString(9)))
		);
	}

	public static List<Area> all(Database db) throws SQLException {
		return db.selectAll(Request.AREAS_ALL, areaSqlMapper);
	}

	public static Optional<Area> withId(Database db, long id) throws SQLException {
		return db.selectOne(Request.AREAS_ID,
		                    statement -> statement.setLong(1, id),
		                    areaSqlMapper);
	}

	public static Area insert(Database db, String name) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(name);
		if (name.isBlank())
			throw new InvalidDataException("Tried to insert an area with a blank name");

		long lastId;
		try (var transaction = db.start()) {

			var result = transaction.update(Request.AREAS_INSERT, statement -> {
				statement.setString(1, name);
			});

			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the area: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new Area(lastId, name);
	}
}

package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.sql.Transaction;
import fr.ipb.soiree.utils.InvalidDataException;
import fr.ipb.soiree.utils.Pair;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class Message {

	private final long id;
	private final Enrollment enrollment;
	private final String content;
	private final Timestamp time;

	Message(long id, Enrollment enrollment, String content, Timestamp time) {
		this.id = id;
		this.enrollment = enrollment;
		this.content = Objects.requireNonNull(content);
		this.time = Objects.requireNonNull(time);
	}

	public long getId() {
		return id;
	}

	public Enrollment getEnrollment() {
		return enrollment;
	}

	public String getContent() {
		return content;
	}

	public Timestamp getTime() {
		return time;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Message)) return false;
		Message message = (Message) o;
		return id == message.id && Objects.equals(enrollment,
		                                          message.enrollment) && content.equals(
				message.content) && time.equals(message.time);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, enrollment, content, time);
	}

	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", enrollmentId=" + enrollment +
				", content='" + content + '\'' +
				", time=" + time +
				'}';
	}

	private static Database.SqlMapper<Pair<Message, Student>> messageMapperFromDB(Database db) {
		return (cols) -> {
			long enrollmentID = cols.getLong(2);
			return new Pair<>(new Message(
					cols.getLong(1),
					Enrollment.withId(db, enrollmentID).orElseThrow(
							() -> new SQLException(
									"Used Enrollment.ID does not exist: " + enrollmentID)),
					cols.getString(3),
					cols.getTimestamp(4)
			),
			                  new Student(cols.getLong(5),
			                              cols.getString(6),
			                              cols.getString(7),
			                              cols.getString(8),
			                              cols.getString(9)));
		};
	}

	public static List<Pair<Message, Student>> withTravel(Database db, Travel travel) throws SQLException {
		Objects.requireNonNull(db);
		Database.SqlMapper<Pair<Message, Student>> messageMapper = messageMapperFromDB(db);
		Transaction.SetParameters params = statement -> statement.setLong(1, travel.getId());
		return db.selectAll(Request.MESSAGES_WITH_TRAVEL, params, messageMapper);
	}

	public static Message insert(Database db, Enrollment enrollment, String content, Timestamp time) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(enrollment);
		Objects.requireNonNull(content);
		Objects.requireNonNull(time);

		long lastId;
		try (var transaction = db.start()) {

			var result = transaction.update(Request.MESSAGE_INSERT, statement -> {
				statement.setLong(1, enrollment.getId());
				statement.setString(2, content);
				statement.setTimestamp(3, time);
			});

			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the Message: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new Message(lastId, enrollment, content, time);
	}

}

package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.sql.Transaction;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

final public class Comment extends Message {

	private final int rating;
	private final List<Student> mentions;

	Comment(long id, Enrollment enrollmentID, String content, Timestamp time, int rating, List<Student> mentions) {
		super(id, enrollmentID, content, time);
		this.rating = rating;
		this.mentions = mentions;
	}

	Comment(long id, Enrollment enrollmentID, String content, Timestamp time, int rating) {
		this(id, enrollmentID, content, time, rating, Collections.emptyList());
	}

	public OptionalInt getRating() {
		return rating == -1 ? OptionalInt.of(rating) : OptionalInt.empty();
	}

	public List<Student> getMentions() {
		return mentions;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Comment comment = (Comment) o;
		return rating == comment.rating;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), rating);
	}

	private static Database.SqlMapper<Comment> commentMapperFromDB(Database db) {
		return (cols) -> {
			long enrollmentID = cols.getLong(2);
			int rating = cols.getInt(5);
			if (cols.wasNull())
				rating = -1;
			return new Comment(
					cols.getLong(1),
					Enrollment.withId(db, enrollmentID).orElseThrow(
							() -> new SQLException(
									"Used Enrollment.ID does not exist: " + enrollmentID)),
					cols.getString(3),
					cols.getTimestamp(4),
					rating
			);
		};
	}

	public static Set<Comment> allCommentsAboutMe(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);
		Database.SqlMapper<Comment> commentMapper = commentMapperFromDB(db);

		Transaction.SetParameters params = statement -> statement.setLong(1, id);
		return new HashSet<>(db.selectAll(Request.ALL_COMMENTS_ABOUT_ME, params, commentMapper));
	}

	public static Set<Comment> withTravelComments(Database db, Travel travel) throws SQLException {
		Objects.requireNonNull(travel);

		Transaction.SetParameters params = statement -> statement.setLong(1, travel.getId());
		return new HashSet<>(
				db.selectAll(Request.COMMENTS_WITH_TRAVEL, params, commentMapperFromDB(db)));
	}
}

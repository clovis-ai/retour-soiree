package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.sql.Transaction;
import fr.ipb.soiree.utils.InvalidDataException;
import fr.ipb.soiree.utils.Pair;
import fr.ipb.soiree.utils.UnorderedPair;

import java.sql.SQLException;
import java.util.*;

final public class Student {

	private final long id;
	private final String firstName;
	private final String lastName;
	private final String phoneNumber;
	private final String email;

	Student(long id, String firstName, String lastName, String phoneNumber, String email) {
		this.id = id;
		this.firstName = Objects.requireNonNull(firstName);
		this.lastName = Objects.requireNonNull(lastName);
		this.phoneNumber = phoneNumber;
		this.email = Objects.requireNonNull(email);
	}

	public long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Student student = (Student) o;
		return id == student.id &&
				firstName.equals(student.firstName) &&
				lastName.equals(student.lastName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName);
	}

	@Override
	public String toString() {
		return "Student{" +
				"id=" + id +
				", first_name='" + firstName + '\'' +
				", last_name='" + lastName + '\'' +
				", email='" + email + '\'' +
				", phone_number='" + phoneNumber + '\'' +
				'}';
	}

	private static final Database.SqlMapper<Student> fullMapper = cols -> new Student(
			cols.getLong(1),
			cols.getString(2),
			cols.getString(3),
			cols.getString(4),
			cols.getString(5));
	private static final Database.SqlMapper<Pair<Student, Pair<Integer, Float>>> statMapper = cols -> new Pair<>(
			new Student(
					cols.getLong(1),
					cols.getString(2),
					cols.getString(3),
					cols.getString(4),
					cols.getString(5)),
			new Pair<>(
					cols.getInt(6),
					cols.getObject(7) == null ? -1 : cols.getFloat(7))
	);

	public static List<Student> all(Database db) throws SQLException {
		Objects.requireNonNull(db);
		return db.selectAll(Request.STUDENTS_ALL, fullMapper);
	}

	public static List<Student> withFirstName(Database db, String firstName) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(firstName);

		Transaction.SetParameters params = statement -> statement.setString(1, firstName);

		return db.selectAll(Request.STUDENTS_FIRST_NAME, params, fullMapper);
	}

	public static List<Student> withLastName(Database db, String lastName) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(lastName);

		Transaction.SetParameters params = statement -> statement.setString(1, lastName);

		return db.selectAll(Request.STUDENTS_LAST_NAME, params, fullMapper);
	}

	/**
	 * Selects the students with given email. Returns an Optional either empty or populated with the
	 * student if found.
	 *
	 * @param db    The database to connect to.
	 * @param email The email used to look for the student.
	 * @return An Optional either empty or populated with the student if found.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or if a script fails.
	 */
	public static Optional<Student> withEmail(Database db, String email) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(email);

		Transaction.SetParameters params = statement -> statement.setString(1, email);

		return db.selectOne(Request.STUDENTS_EMAIL, params, fullMapper);
	}

	/**
	 * Selects the student with given id. Returns an Optional to consider the case where no student
	 * were found.
	 *
	 * @param db The database to connect to.
	 * @param id The id of the student to look for.
	 * @return An Optional populated with a student if found, empty otherwise.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Optional<Student> withId(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);

		Transaction.SetParameters params = statement -> statement.setLong(1, id);

		return db.selectOne(Request.STUDENTS_ID, params, fullMapper);
	}

	/**
	 * Inserts given student into given database using given elements.
	 *
	 * @param db          The database to insert to.
	 * @param firstName   The student's first name.
	 * @param lastName    The student's last name.
	 * @param phoneNumber The student's phone number.
	 * @param email       The student's email.
	 * @return The inserted student with the actual id managed by the database.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Student insert(Database db, String firstName, String lastName, String phoneNumber, String email) throws SQLException {
		Objects.requireNonNull(db);
		Objects.requireNonNull(firstName);
		Objects.requireNonNull(lastName);
		Objects.requireNonNull(email);
		if (firstName.isBlank() || lastName.isBlank() || email.isBlank())
			throw new InvalidDataException("Tried to insert an invalid student");
		if (phoneNumber != null && phoneNumber.isBlank())
			throw new InvalidDataException(
					"Tried to insert a student with an empty phone number (must be either null or like +XX[X..]");

		long lastId;
		try (var transaction = db.start()) {

			var result = transaction.update(Request.STUDENTS_INSERT, statement -> {
				statement.setString(1, firstName);
				statement.setString(2, lastName);
				statement.setString(3, phoneNumber);
				statement.setString(4, email);
			});

			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the student: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new Student(lastId, firstName, lastName, phoneNumber, email);
	}

	/**
	 * Returns all student information as well as their number of travels and average rating.
	 *
	 * @param db The database to connect to.
	 * @return A map of which the keys are students and values are pairs containing the number of
	 * travels (left) and the average rating (right). If the student has no ratings, the average
	 * rating returned is -1.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Map<Student, Pair<Integer, Float>> allStudentsWithTravelsAndGrade(Database db) throws SQLException {
		Map<Student, Pair<Integer, Float>> m = new HashMap<>();
		for (Pair<Student, Pair<Integer, Float>> pair : db.selectAll(
				Request.ALL_STUDENTS_WITH_TRAVELS_AND_GRADE, statMapper)) {
			m.put(pair.left, pair.right);
		}
		return m;
	}

	/**
	 * Finds students who travelled or will travel together.
	 *
	 * @param db The database that will be queried
	 * @return A set of pairs of students.
	 * @throws SQLException If something goes wrong with the database.
	 */
	public static Set<UnorderedPair<Student>> travelledTogether(Database db) throws SQLException {
		Database.SqlMapper<UnorderedPair<Student>> mapper = cols -> new UnorderedPair<>(
				new Student(cols.getLong(1),
				            cols.getString(2),
				            cols.getString(3),
				            cols.getString(4),
				            cols.getString(5)),
				new Student(cols.getLong(6),
				            cols.getString(7),
				            cols.getString(8),
				            cols.getString(9),
				            cols.getString(10)
				)
		);

		var results = db.selectAll(Request.STUDENTS_TRAVELLED_TOGETHER, mapper);

		return new HashSet<>(results);
	}

	public static Set<Travel> allTravelsNotWithdrawn(Database db, long studentId) throws SQLException {
		Objects.requireNonNull(db);

		Transaction.SetParameters params = statement -> {
			statement.setLong(1, studentId);
		};

		return new HashSet<>(
				db.selectAll(Request.STUDENT_TRAVELS_NOT_WITHDRAWN, params,
				             Travel.travelMapperFromDB(db)));
	}
}

package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.Database;
import fr.ipb.soiree.sql.Request;
import fr.ipb.soiree.sql.Transaction;
import fr.ipb.soiree.utils.InvalidDataException;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

final public class Event {

	private final long id;
	private final String description;
	private final Timestamp start;
	private final Timestamp end;
	private final Place place;

	Event(long id, String description, Timestamp start, Timestamp end, Place place) {
		this.id = id;
		this.description = Objects.requireNonNull(description);
		this.start = Objects.requireNonNull(start);
		this.end = Objects.requireNonNull(end);
		this.place = place;
	}

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public Timestamp getStart() {
		return start;
	}

	public Timestamp getEnd() {
		return end;
	}

	public Place getPlace() {
		return place;
	}

	@Override
	public String toString() {
		return "Event{" +
				"id=" + id +
				", description='" + description + '\'' +
				", start=" + start +
				", end=" + end +
				", place=" + place +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Event event = (Event) o;
		return id == event.id &&
				description.equals(event.description) &&
				start.equals(event.start) &&
				end.equals(event.end) &&
				Objects.equals(place, event.place);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, description, start, end, place);
	}

	/**
	 * Returns the event with the given id.
	 *
	 * @param db The database to connect to.
	 * @param id The event's id.
	 * @return An optional populated with an event if found, empty otherwise.
	 * @throws SQLException Thrown whenever an error occurred in the communication with the database
	 *                      or a script fails.
	 */
	public static Optional<Event> withID(Database db, long id) throws SQLException {
		Objects.requireNonNull(db);

		Database.SqlMapper<Event> eventMapper = cols -> {
			long placeID = cols.getLong(5);
			return new Event(
					cols.getLong(1),
					cols.getString(2),
					cols.getTimestamp(3),
					cols.getTimestamp(4),
					Place.withID(db, placeID).orElseThrow(
							() -> new SQLException("Used Place.ID does not exist: " + placeID))
			);
		};

		Transaction.SetParameters params = statement -> statement.setLong(1, id);
		return db.selectOne(Request.EVENT_WITH_ID, params, eventMapper);
	}

	public static List<Event> all(Database db) throws SQLException {
		return db.selectAll(Request.EVENT_ALL, results -> {
			var placeId = results.getLong(5);
			return new Event(
					results.getLong(1),
					results.getString(2),
					results.getTimestamp(3),
					results.getTimestamp(4),
					Place.withID(db, placeId)
							.orElseThrow(() -> new InvalidDataException(
									"Could not find Place with ID: " + placeId))
			);
		});
	}

	public static Event insert(Database db, String description, Timestamp start, Timestamp end, Place place) throws SQLException {
		Objects.requireNonNull(description);
		Objects.requireNonNull(start);
		Objects.requireNonNull(end);
		Objects.requireNonNull(place);

		long lastId;
		try (var transaction = db.start()) {

			var result = transaction.update(Request.EVENT_INSERT, statement -> {
				statement.setString(1, description);
				statement.setTimestamp(2, start);
				statement.setTimestamp(3, end);
				statement.setLong(4, place.getId());
			});

			if (result != 1)
				throw new InvalidDataException(
						"Could not insert the event: " + result + " line·s would be modified (aborted)");

			lastId = transaction.lastId();
			transaction.commit();
		}

		return new Event(lastId, description, start, end, place);
	}

}

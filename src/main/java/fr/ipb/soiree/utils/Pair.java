package fr.ipb.soiree.utils;

import java.util.Objects;

/**
 * A simple ordered pairs of values, which respects the contracts of {@link Object#equals(Object)}
 * and {@link Object#hashCode()}.
 *
 * @param <A> The type of the first element.
 * @param <B> The type of the second element.
 */
public class Pair<A, B> {

	public final A left;
	public final B right;

	public Pair(A left, B right) {
		this.left = Objects.requireNonNull(left);
		this.right = Objects.requireNonNull(right);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Pair<?, ?> pair = (Pair<?, ?>) o;
		return left.equals(pair.left) &&
				right.equals(pair.right);
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public String toString() {
		return "Pair{" +
				"left=" + left +
				", right=" + right +
				'}';
	}
}

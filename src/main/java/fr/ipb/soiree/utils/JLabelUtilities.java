package fr.ipb.soiree.utils;

import javax.swing.*;
import java.awt.*;

public class JLabelUtilities {

	public static void resizeLabel(JLabel label, int size) {
		Font labelFont = label.getFont();
		label.setFont(new Font(labelFont.getName(), Font.PLAIN, size));
	}
}

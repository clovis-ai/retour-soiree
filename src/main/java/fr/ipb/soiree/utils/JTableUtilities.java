package fr.ipb.soiree.utils;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import java.util.Objects;

public class JTableUtilities {

	public static void setCellsAlignment(JTable table, int alignment) {
		Objects.requireNonNull(table);

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(alignment);

		TableModel tableModel = table.getModel();

		for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
			table.getColumnModel().getColumn(columnIndex).setCellRenderer(rightRenderer);
		}
	}

	public static void setCellsMinimumWidth(JTable table, int width) {
		Objects.requireNonNull(table);

		TableModel tableModel = table.getModel();

		for (int columnIndex = 1; columnIndex < tableModel.getColumnCount(); columnIndex++) {
			table.getColumnModel().getColumn(columnIndex).setMinWidth(width);
		}
	}
}

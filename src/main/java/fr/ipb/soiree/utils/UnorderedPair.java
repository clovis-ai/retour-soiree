package fr.ipb.soiree.utils;

import java.util.Objects;

/**
 * A pair of two elements which doesn't care about order:
 * <p>
 * A pair of <code>(1, 2)</code> is equal (according to {@link #equals(Object)} and {@link
 * #hashCode()}) to a pair of <code>(2, 1)</code>. This is only true if the stored class correctly
 * implements {@link Object#equals(Object)} and {@link Object#hashCode()}.
 * <p>
 * Use {@link #reverse()} to get the opposite pair (which is equal with this one).
 *
 * @param <T> The type of the stored elements.
 */
public class UnorderedPair<T> {

	private final T first;
	private final T second;

	public UnorderedPair(T first, T second) {
		Objects.requireNonNull(first);
		Objects.requireNonNull(second);
		this.first = first;
		this.second = second;
	}

	public T getFirst() {
		return first;
	}

	public T getSecond() {
		return second;
	}

	/**
	 * Reverses this pair
	 *
	 * @return A new pair which corresponds <code>(original.second, original.first)</code>
	 */
	public UnorderedPair<T> reverse() {
		return new UnorderedPair<>(second, first);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UnorderedPair<?> that = (UnorderedPair<?>) o;
		return (first.equals(that.first) &&
				second.equals(that.second))
				|| (first.equals(that.second) &&
				second.equals(that.first));
	}

	@Override
	public int hashCode() {
		return first.hashCode() + second.hashCode(); // symmetrical
	}

	@Override
	public String toString() {
		return "(" + first +
				", " + second +
				')';
	}
}

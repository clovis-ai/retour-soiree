-- Withdraw the enrollment with the given Id

update Enrollment
set withdrawn = true
where ID = ?;

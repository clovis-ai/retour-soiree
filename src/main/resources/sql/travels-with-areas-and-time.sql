-- return travels that have given
-- departure area, arrival area and start time

select T.ID, T.start_time, T.dep_place, T.arr_place, T.event, PDEP.area, PARR.area
from Travel T
join Place PDEP on PDEP.ID = T.dep_place
join Place PARR on PARR.ID = T.arr_place
where PDEP.area = ?
  and PARR.area = ?
  and date(T.start_time) = ?;

-- Unwithdraw the enrollment with the given Id

update Enrollment
set withdrawn = false
where ID = ?;

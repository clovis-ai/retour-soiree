-- Finds comments from a specific travel

select C.ID, M.enrollment, M.content, M.time, C.rating
from Comment C
join Message M on M.ID = C.ID
join Enrollment E on E.ID = M.enrollment
join Travel T on T.ID = E.travel
where T.ID = ?;

-- return the event having the given id

select E.ID, E.description, E.start_time, E.end_time, E.place
from Event E
where E.ID = ?;

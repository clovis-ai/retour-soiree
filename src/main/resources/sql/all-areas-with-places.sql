-- For each Area, counts the number of Places

select Area.ID, Area.name, count(P.ID)
from Area
join Place P on Area.ID = P.area
group by Area.ID;

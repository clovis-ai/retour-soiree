-- Inserts a new event

insert into Event(description, start_time, end_time, place)
values (?, ?, ?, ?);

-- Finds the enrollment with given student and travel
-- Student id and Travel id

select ID, withdrawn, student, travel
from Enrollment
where student = ?
  and travel = ?;

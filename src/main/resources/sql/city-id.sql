-- Finds a City with a given ID.

select ID, zip_code, name
from City
where ID = ?;

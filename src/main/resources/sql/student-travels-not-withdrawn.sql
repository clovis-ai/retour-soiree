-- Returns the travels the given student is enrolled to

select T.ID, start_time, dep_place, arr_place, event
from Travel T
join Enrollment E on T.ID = E.travel
where E.student = ?
  and E.withdrawn = false;

-- For each Area, gives the most popular place

select Area.ID,
       Area.name,
       Place.ID,
       Place.address,
       Place.area,
       Place.city,
       City.ID,
       City.zip_code,
       City.name
from (select P.AID as AID, max(NB_TOT) as MAX
      from (select Place.area as AID, count(T.ID) as NB_TOT
            from Place
            join Travel T on Place.ID = T.dep_place or Place.ID = T.arr_place
            group by Place.ID, Place.area) P
      group by P.AID) P1
join (select Place.ID as PID, Place.area as AID, count(T.ID) as NB_TOT
      from Place
      join Travel T on Place.ID = T.dep_place or Place.ID = T.arr_place
      group by Place.ID, Place.area) P2
     on P2.NB_TOT = P1.MAX and P2.AID = P1.AID
join
Area
on Area.ID = P1.AID
join
Place
on P2.PID = Place.ID
join City on Place.city = City.ID
;

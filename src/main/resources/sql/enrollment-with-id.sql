-- Finds all enrollments with a given ID.

select ID, withdrawn, student, travel
from Enrollment
where ID = ?;

-- Select all students with a specific name

select ID, first_name, last_name, phone_number, email
from Student
where Student.first_name = ?;

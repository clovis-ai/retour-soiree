-- Finds students who travelled/will travel together

select S1.ID,
       S1.first_name,
       S1.last_name,
       S1.phone_number,
       S1.email,
       S2.ID,
       S2.first_name,
       S2.last_name,
       S2.phone_number,
       S2.email
from Student S1
join Enrollment E1 on S1.ID = E1.student
join Travel T on T.ID = E1.travel
join Enrollment E2 on E2.travel = T.ID
join Student S2 on S2.ID = E2.student
where S1.ID < S2.ID;

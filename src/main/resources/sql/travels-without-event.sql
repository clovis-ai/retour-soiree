-- return travels without events

select T.ID, T.start_time, T.dep_place, T.arr_place, T.event
from Travel T
where T.event IS NULL;

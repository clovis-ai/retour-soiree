-- return the travel having the given event

select ID, start_time, dep_place, arr_place, event
from Travel
where Event = ?;

-- return the travel having the start_time around 24h of the given date

select ID, start_time, dep_place, arr_place, event
from Travel
where abs(datediff(Travel.start_time, ?)) < 1;

-- For each Travel, counts the number of Students enrolled

select T.ID,
       T.start_time,
       T.dep_place,
       T.arr_place,
       T.event,
       coalesce(sum(1 - E.withdrawn), 0) as ENROLLED
from Travel T
left outer join Enrollment E on T.ID = E.travel
group by T.ID;

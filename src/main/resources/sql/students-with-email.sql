-- Select all students with a specific email address

select ID, first_name, last_name, phone_number, email
from Student
where Student.email = ?;

-- Finds all areas with a given ID.

select ID, name
from Area
where ID = ?;

-- Returns every comment that mentions the given student

select C.ID, M.enrollment, M.content, M.time, C.rating
from CommentMention CM
join Comment C on CM.comment = C.ID
join Message M on C.ID = M.ID
where CM.student = ?;

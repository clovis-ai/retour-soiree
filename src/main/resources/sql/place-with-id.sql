-- return the place having the given id

select P.ID, P.address, P.area, A.name, P.city, C.zip_code, C.name
from Place P
join Area A on A.ID = P.area
join City C on C.ID = P.city
where P.ID = ?;

-- Finds all messages of a given travel

select M.ID,
       M.enrollment,
       M.content,
       M.time,
       S.ID,
       S.first_name,
       S.last_name,
       S.phone_number,
       S.email
from Message M
join Enrollment E on M.enrollment = E.ID
join Student S on S.ID = E.student
left outer join Comment C on M.ID = C.ID
where E.travel = ?
  and C.ID is null
order by M.time desc;

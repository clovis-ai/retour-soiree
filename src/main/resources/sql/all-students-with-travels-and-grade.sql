-- For each Students, counts the number of Travels and average grade

select S.ID,
       S.first_name,
       S.last_name,
       S.phone_number,
       S.email,
       count(distinct travel) as travels,
       avg(rating)            as rating
from Student S
left join
(select distinct S.ID     as student,
                 T.ID     as travel,
                 C.rating as rating
 from Student S
 join Enrollment E on E.student = S.ID
 join CommentMention CM on CM.student = E.student
 join Travel T on T.ID = E.travel
 join Comment C on CM.comment = C.ID
 where T.start_time < now()
) DER on S.ID = DER.student
group by S.ID;

-- return the travel having the dep_place in the given area

select Travel.ID, start_time, dep_place, arr_place, event
from Travel
join Place P on Travel.dep_place = P.ID
join Area on P.area = Area.ID
where Area.ID = ?;

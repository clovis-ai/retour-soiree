package fr.ipb.soiree.utils;

import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UnorderedPairTest {

	@Test
	public void testIntegers() {
		var p1 = new UnorderedPair<>(1, 2);
		var p2 = new UnorderedPair<>(2, 1);
		var p3 = new UnorderedPair<>(2, 1);
		var p4 = new UnorderedPair<>(5, 6);

		assertEquals(p1.hashCode(), p2.hashCode());
		assertEquals(p1, p2);

		assertEquals(p2.hashCode(), p3.hashCode());
		assertEquals(p2, p3);

		assertNotEquals(p2.hashCode(), p4.hashCode());
		assertNotEquals(p3, p4);
	}

	@Test
	public void testHashSet() {
		HashSet<UnorderedPair<Integer>> set = new HashSet<>();

		var p1 = new UnorderedPair<>(1, 2);
		set.add(p1);
		assertEquals(1, set.size());

		set.add(p1);
		assertEquals(1, set.size());

		var p2 = new UnorderedPair<>(2, 1);
		set.add(p2);
		assertEquals(1, set.size());

		var p3 = new UnorderedPair<>(5, 6);
		set.add(p3);
		assertEquals(2, set.size());
	}

	@Test
	public void testReverse() {
		var p1 = new UnorderedPair<>(1, 2);

		assertEquals(Integer.valueOf(1), p1.getFirst());
		assertEquals(Integer.valueOf(2), p1.getSecond());

		var p2 = p1.reverse();

		assertEquals(Integer.valueOf(2), p2.getFirst());
		assertEquals(Integer.valueOf(1), p2.getSecond());
	}

}

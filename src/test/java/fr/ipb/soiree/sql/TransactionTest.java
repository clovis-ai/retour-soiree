package fr.ipb.soiree.sql;

import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class TransactionTest {

	@Test
	public void getAllStudents() throws SQLException {
		try (var db = DatabaseTest.getNew()) {
			Transaction t = db.start();
			var names = new ArrayList<String>(4);
			try (var results = t.select(Request.STUDENTS_ALL)) {
				while (results.next()) {
					names.add(results.getString(3));
				}
			}
			for (var name : new String[]{"Gond", "Andricque", "Canet", "Ben Salah"})
				assertTrue("One of the students should be named '" + name + "'.",
				           names.contains(name));
			t.abort();
		}
	}

	@Test
	public void checkStudents() throws SQLException {
		try (var db = DatabaseTest.getNew()) {
			Transaction t = db.start();
			try (var results = t.select(Request.STUDENTS_LAST_NAME,
			                            (it) -> it.setString(1, "Gond"))) {
				results.next();
				assertEquals("Gond", results.getString(3));

				assertFalse(results.next());
			}
			t.abort();
		}
	}

	@Test
	public void lastId() throws SQLException {
		try (var db = DatabaseTest.getNew()) {
			Transaction t = db.start();
			assertTrue(t.lastId() >= 0);
		}
	}
}

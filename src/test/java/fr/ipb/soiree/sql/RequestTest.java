package fr.ipb.soiree.sql;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class RequestTest {

	@Test
	public void allRequestsStartWithUse() {
		for (Request request : Request.values()) {
			String[] lines = request.text.split("\n");

			var instructionEnds = Arrays.stream(lines)
					.filter(line -> !line.startsWith("--") && !line.isEmpty())
					.filter(line -> line.endsWith(";"))
					.count();

			assertEquals(
					"The script " + request + " has the wrong number of instructions (semi-colons).",
					1, instructionEnds);
		}
	}

}

package fr.ipb.soiree.sql;

import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class DatabaseTest {

	@Test
	public void loadAndClose() {
		var database = Database.connectAuto();
		database.close();
	}

	@Test
	public void connectToFakeDatabase() {
		assertThrows(RuntimeException.class, () -> {
			var database = new Database("300.300.300.300", 0, "no", "no");
			database.close();
		});
	}

	public static Database getNew() {
		// In case we want to use a different database for tests
		return Database.connectAuto();
	}

}

package fr.ipb.soiree.ui;

import org.junit.Test;

import java.util.ArrayList;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class SoireeTableModelTest {

	@Test
	public void instanciation() {
		String[] cols = {"1", "2"};
		var data = new ArrayList<String>();
		data.add("This,is a test");
		data.add("Another,string");
		data.add("Last,one");
		Function<String, Object[]> mapper = (String str) -> str.split(",", 1);
		var model = new SoireeTableModel<>(cols, data, mapper);

		assertEquals(2, model.getColumnCount());
		assertEquals(3, model.getRowCount());
		assertEquals("1", model.getColumnName(0));
		assertEquals("2", model.getColumnName(1));
	}

	@Test
	public void valueRetrieval() {
		String[] cols = {"1", "2"};
		var data = new ArrayList<String>();
		data.add("This,is a test");
		data.add("Another,string");
		data.add("Last,one");
		Function<String, Object[]> mapper = (String str) -> str.split(",", 2);
		var model = new SoireeTableModel<>(cols, data, mapper);

		assertEquals("This", model.getValueAt(0, 0));
		assertEquals("is a test", model.getValueAt(0, 1));
		assertEquals("Another", model.getValueAt(1, 0));
		assertEquals("string", model.getValueAt(1, 1));
		assertEquals("Last", model.getValueAt(2, 0));
		assertEquals("one", model.getValueAt(2, 1));

		assertThrows(IndexOutOfBoundsException.class, () -> model.getValueAt(-1, 0));
		assertThrows(IndexOutOfBoundsException.class, () -> model.getValueAt(3, 0));
		assertThrows(IndexOutOfBoundsException.class, () -> model.getValueAt(0, -1));
		assertThrows(IndexOutOfBoundsException.class, () -> model.getValueAt(0, 2));
		assertThrows(IndexOutOfBoundsException.class, () -> model.getValueAt(-1, -1));
		assertThrows(IndexOutOfBoundsException.class, () -> model.getValueAt(3, 2));
	}
}

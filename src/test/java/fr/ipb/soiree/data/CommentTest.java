package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.DatabaseTest;
import org.junit.Test;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class CommentTest {

	@Test
	public void allCommentsAboutMe() throws SQLException {
		Timestamp travelTime = Timestamp.valueOf("2020-10-06 09:30:00");
		Timestamp commentTime = Timestamp.valueOf("2020-10-06 10:00:00");

		City city1 = new City(1, 33000, "Bordeaux");

		Area area1 = new Area(1, "Quinconces");
		Area area2 = new Area(2, "Victoire");

		Place place1 = new Place(1, "66 avenue de Champs-Elysées", area1, city1);
		Place place2 = new Place(2, "12 rue de la Paix", area2, city1);

		Travel travel1 = new Travel(1, travelTime, place1, place2, null);

		Student david = new Student(1, "David", "Gond", null, "dgond@enseirb-matmeca.fr");
		Student ivan = new Student(2, "Ivan", "Canet", null, "icanet@enseirb-matmeca.fr");
		Student maelle = new Student(3, "Maëlle", "Andricque", null,
		                             "mandricque@enseirb-matmeca.fr");
		Student mehdi = new Student(4, "Mehdi", "Ben Salah", null, "mbensalah@enseirb-matmeca.fr");

		Enrollment enr1 = new Enrollment(1, false, david, travel1);
		Enrollment enr5 = new Enrollment(5, false, ivan, travel1);
		Enrollment enr6 = new Enrollment(6, false, mehdi, travel1);

		Comment abtDavid1 = new Comment(1, enr6, "mehdi david", commentTime, 5);
		Comment abtDavid2 = new Comment(5, enr5, "ivan david", commentTime, 5);
		Comment abtIvan1 = new Comment(2, enr6, "mehdi ivan", commentTime, 4);
		Comment abtIvan2 = new Comment(3, enr1, "david ivan", commentTime, 5);
		Comment abtMehdi = new Comment(4, enr1, "david mehdi", commentTime, 4);

		var db = DatabaseTest.getNew();
		var commentsAbtDavid = Comment.allCommentsAboutMe(db, 1);
		var commentsAbtIvan = Comment.allCommentsAboutMe(db, 2);
		var commentsAbtMaelle = Comment.allCommentsAboutMe(db, 3);
		var commentsAbtMehdi = Comment.allCommentsAboutMe(db, 4);

		assertEquals(Set.of(abtDavid1, abtDavid2), commentsAbtDavid);
		assertEquals(Set.of(abtIvan1, abtIvan2), commentsAbtIvan);
		assertEquals(abtMehdi, commentsAbtMehdi.iterator().next());
		assertTrue(commentsAbtMaelle.isEmpty());
	}
}

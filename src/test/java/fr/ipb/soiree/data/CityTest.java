package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.DatabaseTest;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class CityTest {

	@Test
	public void all() throws SQLException {
		var db = DatabaseTest.getNew();

		var cities = City.all(db);
		int count;
		for (var city : cities) {
			assertFalse(city.getName().isBlank());
			assertTrue(city.getZipCode() > 0);
			count = 0; // Checks that a city appears once
			for (var city2 : cities) {
				if (city.equals(city2))
					count++;
			}
			assertEquals("A city can only appear once", 1, count);
		}
	}

	@Test
	public void withId() throws SQLException {
		var db = DatabaseTest.getNew();

		var cityDB = City.withId(db, 2);
		assertTrue(cityDB.isPresent());
		var city = cityDB.get();
		assertEquals(2, city.getId());
		assertEquals(33400, city.getZipCode());
		assertEquals("Talence", city.getName());
	}

	@Test
	public void insert() throws SQLException {
		var db = DatabaseTest.getNew();

		var name = "fake city";
		var zipCode = 123456;

		var insertCity = City.insert(db, zipCode, name);
		assertEquals(zipCode, insertCity.getZipCode());
		assertEquals(name, insertCity.getName());
	}
}

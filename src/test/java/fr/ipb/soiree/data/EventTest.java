package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.DatabaseTest;
import org.junit.Test;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

public class EventTest {

	@Test
	public void withID() throws SQLException {
		var db = DatabaseTest.getNew();

		var eventDB = Event.withID(db, 1);
		assertTrue(eventDB.isPresent());
		var event = Event.withID(db, 1).get();
		assertEquals(1, event.getId());
		assertEquals("qqchoseeirb", event.getDescription());
		assertEquals(Timestamp.valueOf("2020-12-12 00:00:00"), event.getStart());
		assertEquals(Timestamp.valueOf("2020-12-12 01:00:00"), event.getEnd());
		var place = event.getPlace();
		assertNotNull(place);
		var placeDB = Place.withID(db, place.getId());
		assertTrue(placeDB.isPresent());
		assertEquals(place.getId(), placeDB.get().getId());
	}

	@Test
	public void insert() throws SQLException {
		var db = DatabaseTest.getNew();

		var description = "fake event test";
		var startTime = Instant.now().plus(1, ChronoUnit.HOURS);
		var endTime = startTime.plus(3, ChronoUnit.HOURS);
		var start = Timestamp.from(startTime);
		var end = Timestamp.from(endTime);
		var place = Place.withID(db, 1).get();
		var event = Event.insert(db, description, start, end, place);

		assertEquals(description, event.getDescription());
		assertTrue(event.getId() > 2); // 2 being the last event in initial test DB
		assertEquals(start, event.getStart());
		assertEquals(end, event.getEnd());
		assertEquals(place, event.getPlace());
	}

	@Test
	public void insertFake() {
		var db = DatabaseTest.getNew();

		var fake = FakeObjects.event;
		assertThrows(NullPointerException.class, () -> Event.insert(db, fake.getDescription(),
		                                                            fake.getStart(), fake.getEnd(),
		                                                            fake.getPlace()));
	}
}

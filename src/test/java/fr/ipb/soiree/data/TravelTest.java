package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.DatabaseTest;
import fr.ipb.soiree.utils.Pair;
import org.junit.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TravelTest {

	@Test
	public void getTravelsWithAreasAndTime() throws SQLException {
		Area area1 = new Area(1, "Quinconces");
		Area area2 = new Area(2, "Victoire");
		Date date = Date.valueOf("2020-12-12");

		var travels = Travel.withAreasAndTime(DatabaseTest.getNew(), area1, area2, date);

		for (var travel : travels) {
			assertEquals(date, new Date(travel.getTime().getTime()));
			// the first getTime() is that of Travel
			// the second is that of java.sql.Timestamp
			assertEquals(area1, travel.getDeparture().getArea());
			assertEquals(area2, travel.getArrival().getArea());
		}
	}

	@Test
	public void getAllTravelsWithoutEvent() throws SQLException {
		var travels = Travel.withoutEvent(DatabaseTest.getNew());
		for (var travel : travels) {
			assertEquals("query has returned a travel with an event", Optional.empty(),
			             travel.getEvent());
		}
	}

	@Test
	public void statStudentsByTravel() throws SQLException {
		var expected = List.of(
				new Pair<>(1L, 3),
				new Pair<>(2L, 0),
				new Pair<>(3L, 1),
				new Pair<>(4L, 1),
				new Pair<>(5L, 0),
				new Pair<>(6L, 0),
				new Pair<>(7L, 0),
				new Pair<>(8L, 0),
				new Pair<>(9L, 1)
		).stream().collect(Collectors.toMap(e -> e.left, e -> e.right));

		Travel.studentsByTravel(DatabaseTest.getNew())
				.forEach((travel, value) -> {
					if (expected.containsKey(travel.getId()))
						assertEquals("Travel '" + travel.getId() + "', number of students enrolled",
						             expected.get(travel.getId()), value);
				});
	}

	@Test
	public void insert() throws SQLException {
		var db = DatabaseTest.getNew();
		var tomorrow = Timestamp.from(
				Instant.now().plus(1, ChronoUnit.DAYS).truncatedTo(ChronoUnit.SECONDS));
		var fakeArea = Area.insert(db, "fakeArea");
		var fakeArea2 = Area.insert(db, "fakeArea2");
		var fakeCity = City.insert(db, 999999, "fake City");
		var fakeCity2 = City.insert(db, 999999, "fake City 2");
		var fromPlace = Place.insert(db, "1 fake addr", fakeArea, fakeCity);
		var toPlace = Place.insert(db, "2 fake addr", fakeArea2, fakeCity2);
		var beginTime = Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS));
		var endTime = Timestamp.from(Instant.now().plus(2, ChronoUnit.DAYS));
		var event = Event.insert(db, "fake event", beginTime, endTime, toPlace);
		var newTravel = Travel.insert(db, tomorrow, fromPlace, toPlace, event);

		assertEquals(tomorrow, newTravel.getTime());
		assertEquals(fromPlace, newTravel.getDeparture());
		assertEquals(toPlace, newTravel.getArrival());
		assertEquals(event, newTravel.getEvent().get());
	}
}

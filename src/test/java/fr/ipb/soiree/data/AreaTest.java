package fr.ipb.soiree.data;

import com.mysql.cj.conf.ConnectionUrlParser.Pair;
import fr.ipb.soiree.sql.DatabaseTest;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class AreaTest {

	@Test
	public void statAllAreasWithPlaces() throws SQLException {
		var areas = Area.allAreasWithPLaces(DatabaseTest.getNew());
		var places = List.of(new Pair<>(new Area(1, "Quinconces"), 1),
		                     new Pair<>(new Area(2, "Victoire"), 2),
		                     new Pair<>(new Area(3, "Saint-Genès"), 3));
		for (var place : places) {
			assertEquals(
					"Wrong number of places in area '" + place.left.getName() + "'",
					areas.get(place.left),
					place.right
			);
		}
	}

	@Test
	public void statMostPopularPlaces() throws SQLException {
		var expected = List.of(
				new Pair<>(new Area(1, "Quinconces"), List.of(1L)),
				new Pair<>(new Area(2, "Victoire"), List.of(3L)),
				new Pair<>(new Area(3, "Saint-Genès"), List.of(4L, 5L))
		).stream().collect(Collectors.toMap(e -> e.left, e -> e.right));

		var expectedIds = new HashSet<>(
				Arrays.asList(1L, 2L, 3L)); // Used to ignore values from other tests
		Area.mostPopularPlaces(DatabaseTest.getNew())
				.forEach((area, value) -> {
					if (expectedIds.contains(area.getId())) {
						var places = value.stream().map(Place::getId).collect(
								Collectors.toList());
						assertEquals("Area '" + area.getId() + "', most popular areas",
						             expected.get(area), places);
					}
				});
	}

	@Test
	public void insert() throws SQLException {
		var db = DatabaseTest.getNew();
		var name = "fake area";
		var insertedArea = Area.insert(db, name);

		assertEquals(name, insertedArea.getName());
	}
}

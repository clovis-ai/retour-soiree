package fr.ipb.soiree.data;

import fr.ipb.soiree.sql.DatabaseTest;
import fr.ipb.soiree.utils.InvalidDataException;
import fr.ipb.soiree.utils.Pair;
import org.junit.Test;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class StudentTest {

	@Test
	public void equals() throws SQLException {
		var student_email = "dgond@enseirb-matmeca.fr";

		var db = DatabaseTest.getNew();
		var student = Student.withEmail(db, student_email);
		assertTrue(student.isPresent());
		var student2 = Student.withId(db, student.get().getId());
		assertEquals(student, student);
		assertEquals(student, student2);
		assertNotEquals(student, null);
	}

	@Test
	public void getAllStudents() throws SQLException {
		var students = Student.all(DatabaseTest.getNew());
		var lastNames = List.of("Gond", "Canet", "Andricque", "Ben Salah");

		for (var name : lastNames) {
			assertTrue(
					"There should be a student named '" + name + "', but I didn't find one: "
							+ students,
					students.stream().map(Student::getLastName).anyMatch(it -> it.equals(name)));
		}
	}

	@Test
	public void getStudentsByLastName() throws SQLException {
		var students = Student.withLastName(DatabaseTest.getNew(), "Gond");
		Student student = students.get(0);

		assertEquals(1, students.size());
		assertEquals("Gond", student.getLastName());
		assertEquals("David", student.getFirstName());
		assertNull(student.getPhoneNumber());
		assertEquals("dgond@enseirb-matmeca.fr", student.getEmail());
	}

	@Test
	public void getStudentsByFirstName() throws SQLException {
		var students = Student.withFirstName(DatabaseTest.getNew(), "David");
		Student student = students.get(0);

		assertEquals(1, students.size());
		assertEquals("Gond", student.getLastName());
		assertEquals("David", student.getFirstName());
		assertNull(student.getPhoneNumber());
		assertEquals("dgond@enseirb-matmeca.fr", student.getEmail());
	}

	@Test
	public void getStudentWithId() throws SQLException {
		var db = DatabaseTest.getNew();

		var insertedStudent = Student.insert(db,
		                                     "Robert",
		                                     "Dupont",
		                                     "+33631395245",
		                                     "robert.dupont@email.fr");

		var student = Student.withId(db, insertedStudent.getId());
		assertTrue(student.isPresent());

		student = Student.withId(DatabaseTest.getNew(), -1);
		assertFalse(student.isPresent());
	}

	@Test
	public void insertValidStudent() throws SQLException {
		var db = DatabaseTest.getNew();
		var student_email = "robert.dupont@other-email.fr";

		var dbStudent = Student.withEmail(db, student_email);
		assertTrue(dbStudent.isEmpty()); // Check that student can be inserted

		Student.insert(db, "Robert", "Dupont", "+33631395245", student_email);
		var dbInsertedStudent = Student.withEmail(db, student_email);
		assertFalse(dbInsertedStudent.isEmpty()); // Check that student has been inserted
	}

	@Test
	public void insertInvalidStudent() {
		var db = DatabaseTest.getNew();

		assertThrows(InvalidDataException.class,
		             () -> Student.insert(db,
		                                  "",
		                                  "Dupont",
		                                  "+33612345678",
		                                  "rdupont@email.fr"));

		assertThrows(NullPointerException.class,
		             () -> Student.insert(db,
		                                  null,
		                                  "Dupont",
		                                  "+33612345678",
		                                  "rdupont@email.fr"));

		assertThrows(InvalidDataException.class,
		             () -> Student.insert(db,
		                                  "Robert",
		                                  "",
		                                  "+33612345678",
		                                  "rdupont@email.fr"));

		assertThrows(NullPointerException.class,
		             () -> Student.insert(db,
		                                  "Robert",
		                                  null,
		                                  "+33612345678",
		                                  "rdupont@email.fr"));

		assertThrows(InvalidDataException.class,
		             () -> Student.insert(db,
		                                  "Robert",
		                                  "Dupont",
		                                  "+33612345678",
		                                  ""));

		assertThrows(NullPointerException.class,
		             () -> Student.insert(db,
		                                  "Robert",
		                                  "Dupont",
		                                  "+33612345678",
		                                  null));
	}

	@Test
	public void getAllStudentsWithTravelsAndGrade() throws SQLException {
		var students = Student.allStudentsWithTravelsAndGrade(DatabaseTest.getNew());
		Student david = new Student(1, "David", "Gond", null, "dgond@enseirb-matmeca.fr");
		Student ivan = new Student(2, "Ivan", "Canet", null, "icanet@enseirb-matmeca.fr");
		Student maelle = new Student(3, "Maëlle", "Andricque", null,
		                             "mandricque@enseirb-matmeca.fr");
		Student mehdi = new Student(4, "Mehdi", "Ben Salah", null, "mbensalah@enseirb-matmeca.fr");

		assertEquals(new Pair<>(3, 5f), students.get(david));
		assertEquals(new Pair<>(1, 4.5f), students.get(ivan));
		assertEquals(new Pair<>(0, -1f), students.get(maelle));
		assertEquals(new Pair<>(2, 4f), students.get(mehdi));
	}

	@Test
	public void studentsWhoTravelledTogether() throws SQLException {
		var pairs = Student.travelledTogether(DatabaseTest.getNew());

		assertEquals(3, pairs.size());
		var pairsList = new ArrayList<>(pairs);

		assertEquals("Gond", pairsList.get(0).getFirst().getLastName());
		assertEquals("Canet", pairsList.get(0).getSecond().getLastName());

		assertEquals("Canet", pairsList.get(1).getFirst().getLastName());
		assertEquals("Ben Salah", pairsList.get(1).getSecond().getLastName());

		assertEquals("Gond", pairsList.get(2).getFirst().getLastName());
		assertEquals("Ben Salah", pairsList.get(2).getSecond().getLastName());
	}

	@Test
	public void travelsNotWithdrawn() throws SQLException {
		var db = DatabaseTest.getNew();

		var newStudent = Student.insert(db, "fake", "student", null, "fake@student.fr");
		var newStudentTravels = Student.allTravelsNotWithdrawn(db, newStudent.getId());
		assertTrue(newStudentTravels.isEmpty());
		var time = Timestamp.from(
				Instant.now().plus(3, ChronoUnit.DAYS).truncatedTo(ChronoUnit.SECONDS));
		var area = Area.insert(db, "area-test-travelsNotWithdrawn");
		var city = City.insert(db, 99999, "area-test-travelsNotWithdrawn");
		var area2 = Area.insert(db, "area-test-travelsNotWithdrawn2");
		var city2 = City.insert(db, 99999, "area-test-travelsNotWithdrawn2");
		var departure = Place.insert(db, "place-test-travelsNotWithdrawn", area, city);
		var arrival = Place.insert(db, "place-test-travelsNotWithdrawn", area2, city2);
		var travel = Travel.insert(db, time, departure, arrival, null);

		var enrollment = Enrollment.enroll(db, newStudent, travel);
		var enrollmentDB = Enrollment.withStudentAndTravel(db, newStudent, travel);
		assertTrue(enrollmentDB.isPresent());
		assertEquals(enrollment, enrollmentDB.get());

		var allEnrolledTravels = Student.allTravelsNotWithdrawn(db, newStudent.getId());
		assertEquals(1, allEnrolledTravels.size());
	}
}

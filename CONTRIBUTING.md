# Conventions de code

Tout le code et les commits suivent les [conventions BrainDot](https://gitlab.com/braindot/legal/-/tree/master/coding-style).

# Utilisation de GitLab

- [Liste des tâches](https://gitlab.com/clovis-ai/retour-soiree/-/issues)
- [Tableau des tâches](https://gitlab.com/clovis-ai/retour-soiree/-/boards)
- [Avancement du projet](https://gitlab.com/clovis-ai/retour-soiree/-/milestones)

## Gestion des tâches

- Sur la page d'une tâche, on peut commenter sur la manière dont elle peut être implémentée, discuter de son utilité, etc.
- La personne qui décide de réaliser une tâche se désigne comme ‘assignee’ (en haut à droite de la page d'une tâche). Cela permet d'éviter que deux personnes travaillent sur la même tâche.
- Quand on a prévu de travailler bientôt sur une tâche, on la marque comme ‘to do’ : soit dans le menu ‘label’ à droite d'une tâche, soit en glissant la tâche dans la bonne colonne dans le tableau.
- Quand on commence à travailler sur une tâche, on la marque comme ‘doing’ (de la même manière que précédemment).

## Travailler sur une tâche

### Version 1 : avec GitLab

Avantage : pas de configuration, inconvénient : pas de chronométrage du temps pris.

- Sur la page d'une tâche qui nous est attribuée, appuyer sur le bouton ‘create merge request’
- `$ git fetch`
- Une nouvelle branche a été créée, qui commence par le numéro de la tâche suivie de son nom.
- `$ git switch <le nom de la branche>` (pensez à utiliser l'auto-complétion…)
- Vous pouvez travailler dans la branche, faire des commits, etc. Vous pouvez `push` autant de fois que vous le souhaitez, puisqu'une seule personne n'a le droit de toucher à une branche, aucun risque d'avoir des conflits.

### Version 2 : avec IntelliJ

Avantages : chronométrage du temps passé. Pas besoin d'ouvrir un navigateur. Conseillé uniquement si vous êtes à l'aise avec l'IDE.

Configuration :
- [Créer un token](https://gitlab.com/-/profile/personal_access_tokens) avec les autorisations `api`, `read_user` et `read_repository` et le copier quelque part
- Dans la barre du haut, `Tools` → `Tasks & context` → `Configure servers`
- Bouton `+` → `Generic`
- Dans l'onglet 'General':
  - Server URL : `https://gitlab.com/api/v4`
  - Cocher 'login anonymously'
- Dans l'onglet 'Server configuration':
  - Tasks list URL : `{serverUrl}/projects/{project}/issues?private_token={token}`
  - Single issue URL : `{serverUrl}/projects/{project}/issues/{id}?private_token={token}`
  - Response type : `JSON`
  - Assignations :
    - `tasks` → `$`
    - `id` → `iid`
    - `summary` → `title`
    - `description` → `description`
    - `updated` → `updated_at`
    - `created` → `created_at`
    - `closed` → Laisser vide
    - `issueUrl` → `web_url`
    - `singleTask-id` → `iid`
    - `singleTask-summary` → `title`
    - `singleTask-description` → `description`
    - `singleTask-updated` → `updated_at`
    - `singleTask-created` → `created_at`
    - `singleTask-closed` → Laisser vide
  - Dans 'Manage Template Variables' :
    - Ajouter une variable `project` contenant `22022480`
    - Ajouter une variable `token` contenant le Token GitLab
- Dans `Settings` → `Tools` → `Tasks` → `Time tracking`, cocher `Enable time tracking`.

Utilisation :
- `Open task` (mettez-vous un raccourci que vous aimez bien)
- Choisir une des tâches
- Choisir le nom de la branche (pour que GitLab la reconnaisse, il faut qu'elle commence par le numéro de la tâche)
- Lors d'un push, GitLab affiche une URL pour créer une merge request. Ne pas oublier d'écrire `Draft:` si elle n'est pas terminée.

## Review

Quand on a fini de travailler sur une tâche :
- Aller sur la page de la merge request (elle est affichée dans le terminal à chaque `push`)
- En haut à droite, `Mark as ready`
- Sélectionner une personne qui va être responsable de vérifier que le code est correct : en haut à droite, `Assignee`

Étapes de vérifications pour la personne assignée :
- Vérifier que les commits respectent les conventions
- Lire les modifications (onglet `Changes` ou dans avec IntelliJ/Git), vérifier les conventions de code
- Vérifier qu'il n'y a pas de merges dans la branche (sauf bonne raison)
- S'il y a un problème, ajouter un commentaire sur la ligne qui correspond (cliquer en marge de la ligne), ou un commentaire général
- Si tout est bon :
  - Appuyer sur `Modify merge commit`
  - Cocher `Include merge request description`
  - Modifier le message de commit pour qu'il résume la merge request en entier
  - Vérifier que le message contient `Closes #` ou `Fixes #` suivis du numéro de la tâche
  - Si tout est bon, appuyer sur le bouton `Merge`

Si vous n'êtes pas assigné à une merge request, vous pouvez quand même donner votre avis via les commentaires ou le bouton `Approve`.

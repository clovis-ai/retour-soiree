plugins {
	java
	application
	jacoco
	id("org.barfuin.gradle.jacocolog") version "1.2.3"
}

repositories {
	jcenter()
}

dependencies {
	// Logging
	fun slf4j(name: String, version: String = "1.7.30") = "org.slf4j:slf4j-$name:$version"
	fun log4j(name: String, version: String = "2.13.3") = "org.apache.logging.log4j:$name:$version"
	implementation(slf4j("api"))
	implementation(log4j("log4j"))
	implementation(log4j("log4j-slf4j18-impl"))
	implementation("com.jcabi:jcabi-log:0.18.1")

	// MySQL
	implementation("mysql:mysql-connector-java:8.0.22")

	// Swing addons
	implementation("com.github.lgooddatepicker:LGoodDatePicker:11.1.0")

	// Tests
	testImplementation("junit:junit:4.13")
}

application {
	mainClassName = "fr.ipb.soiree.App"
}

tasks.jacocoTestReport {
	dependsOn(tasks.test)
	reports {
		xml.isEnabled = true
		csv.isEnabled = false
		html.isEnabled = true
	}
}

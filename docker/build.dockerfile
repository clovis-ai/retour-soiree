FROM archlinux
MAINTAINER Ivan Canet

# Fix for https://github.com/commercialhaskell/stack/issues/1012
RUN ln -sf /usr/lib/libncursesw.so /usr/lib/libtinfo.so.5

ENV JAVA_HOME=/usr/lib/jvm/default-runtime/

# In order:
# - Standard UNIX tools necessary
# - Java dependencies
# - MySQL dependencies
# - Remove the Pacman cache (in hope of removing some weight)
RUN pacman -Sy --noconfirm \
	make wget lsof git zip \
	jdk-openjdk \
	libaio numactl \
	&& pacman -Scc --noconfirm

# retour-soiree

Projet S7 ENSEIRB 2020

## Équipe du projet
- Mehdi Ben Salah
- Maelle Andricque

- David Gond

- Ivan Canet

## Conventions de code

[Voir CONTRIBUTING.md](CONTRIBUTING.md)

## Compilation & exécution

Les pré-requis pour compiler ce projet sont :
- le Java Development Kit (JDK)
- outils communs UNIX (`make`, `wget`, `sh`, `lsof`, `git`)
- une connexion internet

Ce projet compile uniquement sur Linux. Toutes ces commandes doivent être exécutées dans le dossier racine du projet. En utilisant l'archive du rendu, il faut les exécuter dans le dossier 'code'.
- `make report.pdf` : Compiler le rapport (en utilisant LuaLaTeX)
- `make run` : Lancer l'application
- `make test` : Exécuter les tests (utiliser `make coverage` pour afficher le pourcentage de couverture du code)
- `make javadoc` : Compiler la documentation du code (dans le dossier build/docs/javadoc)
- `make clean` : Nettoyer les fichiers générés
- `make /tmp/rendu.zip` : Générer l'archive pour les profs (elle se trouve dans `/tmp/rendu.zip`)
- `make uninstall` : Supprimer les fichiers générés et l'installation de MySQL (à noter que certains fichiers de Gradle sont stockés dans `~/.gradle` et ne seront pas supprimés par cette commande)

Si vous ne pouvez pas compiler le rapport (par exemple à cause de problèmes dans l'installation de LaTeX), vous pouvez trouver la version la plus récente du rapport [ici](https://clovis-ai.gitlab.io/retour-soiree/index.html). La documentation du code (Javadoc) est disponible sur ce même lien.
